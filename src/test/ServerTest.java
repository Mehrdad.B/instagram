import Client.Controller.Network.SSSocket;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerTest
{
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(8888);
        SSSocket socket = new SSSocket(server.accept());
        System.out.println("someone connected");

        while (true) {
            String s = new String(socket.readMessage());
            System.out.println(s);
        }
    }
}
