package Client.Model;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.Manager.DataBase.FieldDetails;

public class TextMessage
{
    private String senderUsername;
    private String receiverUsername;
    private String text;

    public TextMessage(String senderUsername, String receiverUsername, String text) {
        this.senderUsername = senderUsername;
        this.receiverUsername = receiverUsername;
        this.text = text;
    }

    public DBObject getDBObject()
    {
        return new BasicDBObject()
                .append(FieldDetails.senderUsername.name(), senderUsername)
                .append(FieldDetails.receiverUsername.name(), receiverUsername)
                .append(FieldDetails.text.name(), text);
    }
}
