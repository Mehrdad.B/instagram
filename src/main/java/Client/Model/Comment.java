package Client.Model;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.Manager.DataBase.FieldDetails;

public class Comment
{
    private String writer;
    private String text;
    private long postID;

    public Comment(String writer, String text, long postID) {
        this.writer = writer;
        this.text = text;
        this.postID = postID;
    }

    public DBObject getDBObject()
    {
        return new BasicDBObject()
                .append(FieldDetails.username.name(), writer)
                .append(FieldDetails.text.name(), text)
                .append(FieldDetails.postID.name(), postID);
    }
}
