package Client.Model;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.Manager.DataBase.FieldDetails;

public class User
{
    private String username;
    private String password;
    private String bio;
    private String email;
    private Boolean privacy;
    private int followingsNumber;
    private int followersNumber;
    private int postsNumber;

    public User(String username, String password, String email)
    {
        this.username = username;
        this.password = password;
        this.email = email;

        followersNumber = 0;
        followingsNumber = 0;
        postsNumber = 0;
        privacy = false;
        bio = "";
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getBio() {
        return bio;
    }

    public String getEmail() {
        return email;
    }

    public Boolean getPrivacy() {
        return privacy;
    }

    public int getFollowingsNumber() {
        return followingsNumber;
    }

    public int getFollowersNumber() {
        return followersNumber;
    }

    public int getPostsNumber() {
        return postsNumber;
    }

    public DBObject getDbObject() {
        return new BasicDBObject()
                .append(FieldDetails.username.name(), getUsername())
                .append(FieldDetails.password.name(), getPassword())
                .append(FieldDetails.email.name(), getEmail())
                .append(FieldDetails.bio.name(), getBio())
                .append(FieldDetails.followersNumber.name(), getFollowersNumber())
                .append(FieldDetails.followingNumber.name(), getFollowingsNumber())
                .append(FieldDetails.postsNumber.name(), getPostsNumber())
                .append(FieldDetails.privacy.name(), privacy);
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.username.equals(((User) obj).getUsername());
    }
}
