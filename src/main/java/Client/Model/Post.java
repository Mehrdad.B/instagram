package Client.Model;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.util.List;

public class Post
{
    //todo implement video class extending this class
    private String username;
    private String caption;
    private long likeNumber;
    private Double width;
    private Double length;
    private List<Comment> comments;
    private long postID;
    private byte[] imageBytes ;
    private long requestID ;
    private String requestType ;


    public Post(String username, String caption, long postID, long requestID, String requestType) {
        this.username = username;
        this.caption = caption;
        this.postID = postID;
        this.requestID = requestID;
        this.requestType = requestType;
        likeNumber = 0;
    }

    public DBObject getDBObject ()
    {
        return new BasicDBObject()
                .append("username" , username)
                .append("requestID" , requestID )
                .append("requestType" , requestType )
                .append("imageBytes" , imageBytes)
                .append("postID", postID)
                .append("caption", caption)
                .append("likeNumber", likeNumber) ;
    }
}
