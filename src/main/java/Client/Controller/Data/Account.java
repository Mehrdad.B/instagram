package Client.Controller.Data;

import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Account
{
    public static String username;
    public static String previousPage;
    public static boolean chatPage = false;
    public static boolean privacy = false;
    public static FileChooser fileChooser = new FileChooser();
    public static File file;
    public static List<Pane> panes = new ArrayList<>();
    public static List<Pane> notifPanes = new ArrayList<>();
    public static List<Pane> chatPanes = new ArrayList<>();
    public static List<Pane> commentPanes = new ArrayList<>();
}
