package Client.Controller.ResponseHandling;

import java.util.List;

public class Response
{
    private long requestID;
    private String requestType;
    private boolean successful;
    private String ownerUsername;
    private String otherUsername;
    private String search;
    private String response;
    private int followingNumber;
    private int followersNumber;
    private int postsNumber;
    private boolean privacy;
    private String bio;
    private List<String> list;
    private String followLabel;
    private String blockLabel;
    private boolean visible;
    private String caption;
    private long likeNumber;
    private long postID;
    private String color;
    private String text;
    private String notificationType;


    public long getRequestID() {
        return requestID;
    }

    public String getRequestType() {
        return requestType;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public String getOtherUsername() {
        return otherUsername;
    }

    public String getResponse() {
        return response;
    }

    public int getFollowingNumber() {
        return followingNumber;
    }

    public int getFollowersNumber() {
        return followersNumber;
    }

    public int getPostsNumber() {
        return postsNumber;
    }

    public boolean getPrivacy() {
        return privacy;
    }

    public String getBio() {
        return bio;
    }

    public String getSearch() {
        return search;
    }

    public List getList() {
        return list;
    }

    public String getFollowLabel() {
        return followLabel;
    }

    public String getBlockLabel() {
        return blockLabel;
    }

    public boolean isVisible() {
        return visible;
    }

    public String getCaption() {
        return caption;
    }

    public long getLikeNumber() {
        return likeNumber;
    }

    public String getPostID() {
        return String.valueOf(postID);
    }

    public String getColor() {
        return color;
    }

    public String getText() {
        return text;
    }

    public String getNotificationType() {
        return notificationType;
    }
}
