package Client.Controller.ResponseHandling;

import Client.Controller.Network.RequestCreator;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.View.viewControllers.*;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.ImageCursor;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import org.controlsfx.control.Notifications;
import javafx.stage.Stage;
import sun.nio.ch.Net;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

public class ResponseActions {

    public void signUp(Response response) {
        if (response.isSuccessful()) {
            Platform.runLater(() ->
            {
                try {
                    PagesController.openPage("login");

                    Notifications.create().title("Notification").text("Signup successful !")
                            .darkStyle().position(Pos.BOTTOM_RIGHT).showConfirm();

                    PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else {
            Platform.runLater(() ->
            {
                Notifications.create().title("Notification").text(response.getResponse())
                        .darkStyle().position(Pos.BOTTOM_RIGHT).showError();
            });
        }
    }

    public void logIn(Response response) {
        if (response.isSuccessful()) {
            Account.username = response.getOwnerUsername();
            NetworkController.addRequest(RequestCreator.profilePage(response.getOwnerUsername()),
                    RequestCreator.requestID, NetworkController.requestActions.get(response.getRequestID()));
        } else {
            Platform.runLater(() ->
            {
                Notifications.create().title("Notification").text(response.getResponse())
                        .darkStyle().position(Pos.BOTTOM_RIGHT).showError();
            });
        }
    }

    public void setting(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.openPage("Setting");
                Account.previousPage = "setting";
                settingController.usernameSet.setText(Account.username);
                settingController.biographySet.setText(response.getBio());

                if (response.getPrivacy()) {
                    settingController.privacySet.setText("private");
                    settingController.privacySet.setSelected(true);
                } else {
                    settingController.privacySet.setText("public");
                    settingController.privacySet.setSelected(false);
                }

                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void profilePage(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.getStage().close();
                PagesController.openPage("ProfilePage");

                Account.previousPage = "profile page";

//                if (hasNotif)
//                {
//                    Notifications.create().title(titleNotif).text(textNotif)
//                            .darkStyle().position(Pos.BOTTOM_RIGHT).showInformation();
//                    hasNotif = false;
//                }

                ProfilePageController.usernameSet.setText(Account.username);
                ProfilePageController.postsNumberSet.setText(Integer.toString(response.getPostsNumber()));
                ProfilePageController.followersNumberSet.setText(Integer.toString(response.getFollowersNumber()));
                ProfilePageController.followingNumberSet.setText(Integer.toString(response.getFollowingNumber()));
                ProfilePageController.bioSet.setText(response.getBio());

                Image image = new Image("/icon.png", false);
                ProfilePageController.profilePicSet.setFill(new ImagePattern(image));

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void saveSetting(Response response) {
        Platform.runLater(() ->
        {
            Account.username = response.getOwnerUsername();
            Account.privacy = response.getPrivacy();

            Notifications.create().title("Notification").text(response.getResponse())
                    .darkStyle().position(Pos.BOTTOM_RIGHT).showWarning();
            settingController.usernameSet.setText(Account.username);
            settingController.biographySet.setText(response.getBio());
        });
    }

    public void pageChangePassword(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.openPage("ChangePassword");
                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void changePassword(Response response) {
        Platform.runLater(() ->
        {
            Notifications.create().title("Notification").text(response.getResponse())
                    .darkStyle().position(Pos.BOTTOM_RIGHT).showWarning();
        });
    }

    public void search(Response response) {
        if (response.isSuccessful()) {
            Platform.runLater(() ->
            {
                try {
                    PagesController.openPage("listOfAccounts");
                    Account.previousPage = "search";

                    listOfAccountsController.usernameSet.setText(response.getOwnerUsername());
                    listOfAccountsController.accountSubjectSet.setText("Recent");
                    listOfAccountsController.showListSet.setItems(FXCollections.observableList(response.getList()));

                    PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else {
            Platform.runLater(() ->
            {
                Notifications.create().title("Notification").text(response.getResponse())
                        .darkStyle().position(Pos.BOTTOM_RIGHT).showError();
            });
        }
    }

    public void othersPage(Response response) {
        Platform.runLater(() ->
        {
            if (response.getOtherUsername().equals(response.getOwnerUsername())) {
                NetworkController.addRequest(RequestCreator.profilePage(Account.username), RequestCreator.requestID,
                        NetworkController.requestActions.get(response.getRequestID()));
            } else if (!response.isVisible()) {
                try {
                    PagesController.openPage("othersPagePrivate");
                    Account.previousPage = response.getOtherUsername();

                    othersPagePrivateController.usernameSet.setText(response.getOtherUsername());
                    othersPagePrivateController.bioSet.setText(response.getBio());
                    othersPagePrivateController.postsNumberSet.setText(String.valueOf(response.getPostsNumber()));
                    othersPagePrivateController.followersNumberSet.setText(String.valueOf(response.getFollowersNumber()));
                    othersPagePrivateController.followingNumberSet.setText(String.valueOf(response.getFollowingNumber()));
                    othersPagePrivateController.blockLabelSet.setText(response.getBlockLabel());
                    othersPagePrivateController.followButtonSet.setText(response.getFollowLabel());

                    Image image = new Image("/icon.png", false);
                    othersPagePrivateController.profilePicSet.setFill(new ImagePattern(image));

                    PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    PagesController.openPage("othersPage");
                    Account.previousPage = response.getOtherUsername();

                    othersPageController.usernameSet.setText(response.getOtherUsername());
                    othersPageController.bioSet.setText(response.getBio());
                    othersPageController.postsNumberSet.setText(String.valueOf(response.getPostsNumber()));
                    othersPageController.followersNumberSet.setText(String.valueOf(response.getFollowersNumber()));
                    othersPageController.followingNumberSet.setText(String.valueOf(response.getFollowingNumber()));
                    othersPageController.blockLabelSet.setText(response.getBlockLabel());
                    othersPageController.followButtonSet.setText(response.getFollowLabel());

                    Image image = new Image("/icon.png", false);
                    othersPageController.profilePicSet.setFill(new ImagePattern(image));

                    PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void follow(Response response) {
        NetworkController.addRequest(RequestCreator.othersPage(Account.username, response.getOtherUsername()),
                RequestCreator.requestID, NetworkController.requestActions.get(response.getRequestID()));
    }

    public void unfollow(Response response) {
        NetworkController.addRequest(RequestCreator.othersPage(Account.username, response.getOtherUsername()),
                RequestCreator.requestID, NetworkController.requestActions.get(response.getRequestID()));
    }

    public void block(Response response) {
        NetworkController.addRequest(RequestCreator.othersPage(Account.username, response.getOtherUsername()),
                RequestCreator.requestID, NetworkController.requestActions.get(response.getRequestID()));
    }

    public void showFollowers(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.openPage("listOfAccounts");

                listOfAccountsController.usernameSet.setText(response.getOtherUsername());
                listOfAccountsController.accountSubjectSet.setText("Followers");
                listOfAccountsController.showListSet.setItems(FXCollections.observableList(response.getList()));

                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void showFollowing(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.openPage("listOfAccounts");

                listOfAccountsController.usernameSet.setText(response.getOtherUsername());
                listOfAccountsController.accountSubjectSet.setText("Following");
                listOfAccountsController.showListSet.setItems(FXCollections.observableList(response.getList()));

                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void showBlocked(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.openPage("listOfAccounts");

                listOfAccountsController.usernameSet.setText(response.getOwnerUsername());
                listOfAccountsController.accountSubjectSet.setText("Blocked List");
                listOfAccountsController.showListSet.setItems(FXCollections.observableList(response.getList()));

                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void unblock(Response response) {
        NetworkController.addRequest(RequestCreator.othersPage(Account.username, response.getOtherUsername()),
                RequestCreator.requestID, NetworkController.requestActions.get(response.getRequestID()));
    }

    public void newPost(Response response) {
        try {
            byte[] data = Files.readAllBytes(Account.file.toPath());

            NetworkController.addRequest(data, RequestCreator.requestID,
                    NetworkController.requestActions.get(response.getRequestID()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void savePost(Response response) {

        Platform.runLater(() -> {
            addPostController.captionSet.setText("");

            Notifications.create().title("Notification").text(response.getResponse())
                    .darkStyle().position(Pos.BOTTOM_RIGHT).showWarning();
        });
    }


    public void receivePost(Response response, byte[] bytes) {
        Platform.runLater(() ->
        {
            try {
                Pane pane = FXMLLoader.load(getClass().getResource("/eachPost.fxml"));

                eachPostController.captionSet.setText(response.getCaption());
                eachPostController.userNameSet.setText(response.getOtherUsername());
                eachPostController.postIDSet.setText(response.getPostID());
                eachPostController.likesNumberSet.setText(String.valueOf(response.getLikeNumber()));
                eachPostController.likeSet.setFill(Paint.valueOf(response.getColor()));

                Image image = new Image(new ByteArrayInputStream(bytes));
                eachPostController.imageViewSet.setImage(image);

                Account.panes.add(pane);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void postsEnded(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.openPage("posts");
                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void homePageEnded(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.openPage("homePage");
                Account.previousPage = "home page";
                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void showNotifications(Response response) {
        Platform.runLater(() ->
        {
            Account.previousPage = "notifications";
            for (int i = 0; i < response.getList().size(); i += 2) {
                if (response.getList().get(i).equals("followRequestNotif")) {
                    try {
                        Pane pane = FXMLLoader.load(getClass().getResource("/privateNotification.fxml"));
                        privateNotificationController.notificationSet.setText(String.valueOf(response.getList().get(i + 1)));
                        Account.notifPanes.add(pane);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Pane pane = FXMLLoader.load(getClass().getResource("/publicNotification.fxml"));
                        publicNotificationController.notificationSet.setText(String.valueOf(response.getList().get(i + 1)));
                        Account.notifPanes.add(pane);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                PagesController.openPage("showNotification");
                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void showDirect(Response response) {
        Platform.runLater(() ->
        {
            try {
                Account.previousPage = "direct";
                PagesController.openPage("directMessage");

                directMessageController.usernameSet.setText(response.getOwnerUsername());
                directMessageController.chatListSet.setItems(FXCollections.observableList(response.getList()));

                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void message(Response response) {
        if (!response.isSuccessful())
        {
            Notifications.create().title("Notification").text(response.getResponse()).darkStyle().position(Pos.BOTTOM_RIGHT).showWarning();
        }
        else
        {
            Account.chatPage = true;

            Platform.runLater(() ->
            {
                for (int i = 1; i < response.getList().size(); i += 2) {
                    if (response.getList().get(i).equals(Account.username)) {
                        try {
                            Pane pane = FXMLLoader.load(getClass().getResource("/ownerMessage.fxml"));
                            ownerMessageController.textSet.setText(String.valueOf(response.getList().get(i - 1)));
                            Account.chatPanes.add(pane);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Pane pane = FXMLLoader.load(getClass().getResource("/othersMessage.fxml"));
                            othersMessageController.textSet.setText(String.valueOf(response.getList().get(i - 1)));
                            Account.chatPanes.add(pane);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    PagesController.openPage("chatBox");
                    chatBoxController.otherUsernameSet.setText(response.getOtherUsername());
                    PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void newMessage(Response response) {
        Platform.runLater(() ->
        {
            for (int i = 1; i < response.getList().size(); i += 2) {
                if (response.getList().get(i).equals(Account.username)) {
                    try {
                        Pane pane = FXMLLoader.load(getClass().getResource("/ownerMessage.fxml"));
                        ownerMessageController.textSet.setText(String.valueOf(response.getList().get(i - 1)));
                        Account.chatPanes.add(pane);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Pane pane = FXMLLoader.load(getClass().getResource("/othersMessage.fxml"));
                        othersMessageController.textSet.setText(String.valueOf(response.getList().get(i - 1)));
                        Account.chatPanes.add(pane);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                PagesController.openPage("chatBox");
                chatBoxController.otherUsernameSet.setText(response.getOtherUsername());
                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void showComments(Response response) {
        Platform.runLater(() ->
        {
            for (int i = 0; i < response.getList().size(); i += 2) {
                try {
                    Pane pane = FXMLLoader.load(getClass().getResource("/commentMessage.fxml"));

                    commentMessageController.usernameSet.setText(String.valueOf(response.getList().get(i)) + ":");
                    commentMessageController.textSet.setText(String.valueOf(response.getList().get(i + 1)));

                    Account.commentPanes.add(pane);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                PagesController.openPage("commentBox");
                commentBoxController.otherUsernameSet.setText(response.getOtherUsername());
                commentBoxController.postIDSet.setText(response.getPostID());
                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void forgotPassword(Response response) {
        Platform.runLater(() ->
        {
            Notifications.create().title("Notification").text(response.getResponse())
                    .darkStyle().position(Pos.BOTTOM_RIGHT).showWarning();
        });
    }

    public void showLikes(Response response) {
        Platform.runLater(() ->
        {
            try {
                PagesController.openPage("listOfAccounts");

                listOfAccountsController.usernameSet.setText(response.getOwnerUsername());
                listOfAccountsController.accountSubjectSet.setText("Likes");
                listOfAccountsController.showListSet.setItems(FXCollections.observableList(response.getList()));

                PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void directNotif(Response response) {
        if (Account.chatPage) {
            Account.chatPanes.clear();
            Platform.runLater(() ->
            {
                for (int i = 1; i < response.getList().size(); i += 2) {
                    if (response.getList().get(i).equals(Account.username)) {
                        try {
                            Pane pane = FXMLLoader.load(getClass().getResource("/ownerMessage.fxml"));
                            ownerMessageController.textSet.setText(String.valueOf(response.getList().get(i - 1)));
                            Account.chatPanes.add(pane);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Pane pane = FXMLLoader.load(getClass().getResource("/othersMessage.fxml"));
                            othersMessageController.textSet.setText(String.valueOf(response.getList().get(i - 1)));
                            Account.chatPanes.add(pane);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    PagesController.getStage().close();
                    PagesController.openPage("chatBox");
                    chatBoxController.otherUsernameSet.setText(response.getOtherUsername());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else {
            Platform.runLater(() ->
            {
                Notifications.create().title(response.getOtherUsername()).text(String.valueOf(response.getList().get(0)))
                        .darkStyle().position(Pos.BOTTOM_RIGHT).showInformation();

                if (Account.previousPage.equals("profile page")) {
                    NetworkController.addRequest(RequestCreator.profilePage(Account.username),
                            RequestCreator.requestID, NetworkController.requestActions.get(response.getRequestID()));
                }
            });
        }
    }

    public void onlineNotif(Response response) {
        if (Account.previousPage.equals("notifications")) {
            Account.notifPanes.clear();
            Platform.runLater(() ->
            {
                for (int i = 0; i < response.getList().size(); i += 2) {
                    if (response.getList().get(i).equals("followRequestNotif")) {
                        try {
                            Pane pane = FXMLLoader.load(getClass().getResource("/privateNotification.fxml"));
                            privateNotificationController.notificationSet.setText(String.valueOf(response.getList().get(i + 1)));
                            Account.notifPanes.add(pane);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Pane pane = FXMLLoader.load(getClass().getResource("/publicNotification.fxml"));
                            publicNotificationController.notificationSet.setText(String.valueOf(response.getList().get(i + 1)));
                            Account.notifPanes.add(pane);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    PagesController.getStage().close();
                    PagesController.openPage("showNotification");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else {
            Platform.runLater(() -> {

                Notifications.create().title("Notification").text(String.valueOf(response.getList().get(1)))
                        .darkStyle().position(Pos.BOTTOM_RIGHT).showInformation();


                if (Account.previousPage.equals("profile page")) {
                    NetworkController.addRequest(RequestCreator.profilePage(Account.username),
                            RequestCreator.requestID, NetworkController.requestActions.get(response.getRequestID()));
                }

            });
        }
    }

    public void newProfilePicture(Response response) {
        try {
            byte[] data = Files.readAllBytes(Account.file.toPath());

            NetworkController.addRequest(data, RequestCreator.requestID,
                    NetworkController.requestActions.get(response.getRequestID()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void newProfilePictureBytes(Response response) {
    } //??

    public void receivePicture(Response response, byte[] picture) {
        Platform.runLater(() ->
        {
            try {
                PagesController.getStage().close();
                PagesController.openPage("ProfilePage");
                Account.previousPage = "profile page";

//                if (hasNotif)
//                {
//                    Notifications.create().title(titleNotif).text(textNotif)
//                            .darkStyle().position(Pos.BOTTOM_RIGHT).showInformation();
//                    hasNotif = false;
//                }

                ProfilePageController.usernameSet.setText(Account.username);
                ProfilePageController.postsNumberSet.setText(Integer.toString(response.getPostsNumber()));
                ProfilePageController.followersNumberSet.setText(Integer.toString(response.getFollowersNumber()));
                ProfilePageController.followingNumberSet.setText(Integer.toString(response.getFollowingNumber()));
                ProfilePageController.bioSet.setText(response.getBio());

                Image image = new Image(new ByteArrayInputStream(picture));
                ProfilePageController.profilePicSet.setFill(new ImagePattern(image));


                //   PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));


            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void receiveOthersPicture(Response response, byte[] picture) {
        Platform.runLater(() ->
        {
            if (response.getOtherUsername().equals(response.getOwnerUsername())) {
                NetworkController.addRequest(RequestCreator.profilePage(Account.username), RequestCreator.requestID,
                        NetworkController.requestActions.get(response.getRequestID()));
            } else if (!response.isVisible()) {
                try {
                    PagesController.openPage("othersPagePrivate");
                    Account.previousPage = response.getOtherUsername();

                    othersPagePrivateController.usernameSet.setText(response.getOtherUsername());
                    othersPagePrivateController.bioSet.setText(response.getBio());
                    othersPagePrivateController.postsNumberSet.setText(String.valueOf(response.getPostsNumber()));
                    othersPagePrivateController.followersNumberSet.setText(String.valueOf(response.getFollowersNumber()));
                    othersPagePrivateController.followingNumberSet.setText(String.valueOf(response.getFollowingNumber()));
                    othersPagePrivateController.blockLabelSet.setText(response.getBlockLabel());
                    othersPagePrivateController.followButtonSet.setText(response.getFollowLabel());

                    Image image = new Image(new ByteArrayInputStream(picture));
                    othersPagePrivateController.profilePicSet.setFill(new ImagePattern(image));

                    PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    PagesController.openPage("othersPage");
                    Account.previousPage = response.getOtherUsername();

                    othersPageController.usernameSet.setText(response.getOtherUsername());
                    othersPageController.bioSet.setText(response.getBio());
                    othersPageController.postsNumberSet.setText(String.valueOf(response.getPostsNumber()));
                    othersPageController.followersNumberSet.setText(String.valueOf(response.getFollowersNumber()));
                    othersPageController.followingNumberSet.setText(String.valueOf(response.getFollowingNumber()));
                    othersPageController.blockLabelSet.setText(response.getBlockLabel());
                    othersPageController.followButtonSet.setText(response.getFollowLabel());

                    Image image = new Image(new ByteArrayInputStream(picture));
                    othersPageController.profilePicSet.setFill(new ImagePattern(image));

                    PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void directSearch(Response response) {
        Platform.runLater(() ->
        {
            if (response.isSuccessful()) {
                try {
                    PagesController.openPage("directSearch");

                    directSearchController.usernameSet.setText(response.getOwnerUsername());
                    directSearchController.showListSet.setItems(FXCollections.observableList(response.getList()));

                    PagesController.closePage(NetworkController.requestActions.get(response.getRequestID()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Notifications.create().title("Notification").text(response.getResponse())
                        .darkStyle().position(Pos.BOTTOM_RIGHT).showError();
            }
        });
    }
}
