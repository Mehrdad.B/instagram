package Client.Controller.ResponseHandling;

import Client.Controller.MainController;
import com.google.gson.Gson;

public class JsonParser extends Parser
{
    private Gson gson = new Gson();
    @Override
    public void handle (byte[] bytes)
    {
        String json = new String(bytes) ;
        MainController.setResponse(gson.fromJson(json , Response.class));
    }
}
