package Client.Controller;

import Client.Controller.ResponseHandling.Response;
import Client.Controller.ResponseHandling.ResponseActions;
import Client.Controller.ResponseHandling.ByteParser ;
import Client.Controller.ResponseHandling.Parser ;
import Client.Controller.ResponseHandling.JsonParser ;


public class MainController
{
    private static JsonParser jsonParser = new JsonParser();
    private static ByteParser byteParser = new ByteParser();
    private static Parser parser;
    private static Response response ;
    private static String type;
    public static String nextType;

    public static void setType(String type) {
        MainController.type = type;
    }

    public static void setParser(Parser parser) {
        MainController.parser = parser;
    }

    public static JsonParser getJsonParser() {
        return jsonParser;
    }

    public static void setResponse(Response response)
    {
        MainController.response = response;
        MainController.type = response.getRequestType() ;
    }

    public static void process(byte[] res)
    {

        parser.handle(res);

        ResponseActions responseAction = new ResponseActions();

        switch (MainController.type)
        {
            case "signUp" :
            {
                responseAction.signUp(response);
                break;
            }
            case "logIn" :
            {
                responseAction.logIn(response);
                break;
            }
            case "setting" :
            {
                responseAction.setting(response);
                break;
            }
            case "profilePage" :
            {
                if (response.isSuccessful())
                {
                    nextType = "receivePicture" ;
                    setParser(byteParser);
                }
                else
                    responseAction.profilePage(response);

                break;
            }
            case "receivePicture" :
            {
                responseAction.receivePicture(response , res) ;
                setParser(jsonParser);
                break;
            }
            case "saveSetting" :
            {
                responseAction.saveSetting(response);
                break;
            }
            case "pageChangePassword" :
            {
                responseAction.pageChangePassword(response);
                break;
            }
            case "changePassword" :
            {
                responseAction.changePassword(response);
                break;
            }
            case "search" :
            {
                responseAction.search(response);
                break;
            }
            case "othersPage" :
            {
                if (response.isSuccessful())
                {
                    nextType = "receiveOthersPicture" ;
                    setParser(byteParser);
                }
                else
                    responseAction.othersPage(response);
                break;
            }
            case "receiveOthersPicture" :
            {
                responseAction.receiveOthersPicture(response , res);
                setParser(jsonParser);
                break;
            }
            case "follow" :
            {
                responseAction.follow(response);
                break;
            }
            case "unfollow" :
            {
                responseAction.unfollow(response);
                break;
            }
            case "block" :
            {
                responseAction.block(response);
                break;
            }
            case "unblock" :
            {
                responseAction.unblock(response);
                break;
            }
            case "showFollowers" :
            {
                responseAction.showFollowers(response);
                break;
            }
            case "showFollowing" :
            {
                responseAction.showFollowing(response);
                break;
            }
            case "showBlockeds" :
            {
                responseAction.showBlocked(response);
                break;
            }
            case "newPost" :
            {
                responseAction.newPost(response);
                break;
            }
            case "savePost" :
            {
                responseAction.savePost(response);
                break;
            }
            case "showPost" :
            case "homePage" : {
                nextType = "receivePost" ;
                setParser(byteParser);
                break;
            }
            case "receivePost" :
            {
                setParser(jsonParser);
                responseAction.receivePost(response , res);
                break;
            }
            case "postsEnded" :
            {
                responseAction.postsEnded(response);
                break;
            }
            case "homePageEnded" :
            {
                responseAction.homePageEnded(response);
                break;
            }
            case "showNotifications" :
            case "accept" :
            case "reject" : {
                responseAction.showNotifications(response) ;
                break;
            }
            case "showDirect" :
            {
                responseAction.showDirect(response);
                break;
            }
            case "directSearch" :
            {
                responseAction.directSearch(response);
                break;
            }
            case "message" :
            {
                responseAction.message(response);
                break;
            }
            case "newMessage" :
            {
                responseAction.newMessage(response);
                break;
            }
            case "showComments" :
            case "newComment" : {
                responseAction.showComments(response);
                break;
            }
            case "forgotPassword" :
            {
                responseAction.forgotPassword(response);
                break;
            }
            case "showLikes" :
            {
                responseAction.showLikes(response);
                break;
            }
            case "notification" :
            {
                NotifProcess(response , responseAction);
                break;
            }
            case "newProfilePicture" :
            {
                responseAction.newProfilePicture(response) ;
                break;
            }
            case "newProfilePictureBytes" :
            {
                responseAction.newProfilePictureBytes(response) ;
                break;
            }
        }
    }

    public static void NotifProcess ( Response response , ResponseActions responseAction )
    {
        switch (response.getNotificationType())
        {
            case "direct" :
            {
                responseAction.directNotif(response) ;
                break;
            }
            case "like" :
            case "follow" :
            case "followRequest" :
            case "comment" : {
                responseAction.onlineNotif(response);
                break;
            }

        }
    }
}
