package Client.Controller.Network;

import Client.Controller.MainController;
import Client.Controller.ConnectionValues;
import javafx.event.ActionEvent;
import server.Manager.Network.DataManager;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class NetworkController {

    private final SSSocket socket = new SSSocket(ConnectionValues.host, ConnectionValues.PORT_NUMBER);
    private static BlockingDeque<byte[]> requests;
    private static BlockingDeque<byte[]> responses;
    public static Map<Long, ActionEvent> requestActions;
    private Thread send;
    private Thread get;
    private Thread process;


    public NetworkController() throws IOException, NoSuchPaddingException, NoSuchAlgorithmException {
        MainController.setParser(MainController.getJsonParser());
        send = new Thread(new sendRunnable(socket));
        get = new Thread(new getRunnable(socket));
        process = new Thread(new processRunnable(socket));
        send.start();
        get.start();
        process.start();
        responses = new LinkedBlockingDeque<>();
        requests = new LinkedBlockingDeque<>();
        requestActions = new HashMap<>();
    }

    public static void addRequest(byte[] req, long id, ActionEvent actionEvent) {
        try {
            requests.put(req);
            requestActions.put(id, actionEvent);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class sendRunnable implements Runnable {
        SSSocket socket;

        private sendRunnable(SSSocket socket) throws IOException {
            this.socket = socket;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (requests.isEmpty() == false) {
                        byte[] data = requests.take();
                        data = DataManager.encrypt(data);
                        socket.sendMessage(data);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class getRunnable implements Runnable {
        SSSocket socket;

        private getRunnable(SSSocket socket) throws IOException {
            this.socket = socket;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    byte[] res = socket.readMessage();
                    res = DataManager.decrypt(res);
                    responses.put(res);
                } catch (Exception e) {
                    e.printStackTrace();


                }
            }
        }
    }

    private class processRunnable implements Runnable {
        SSSocket socket;

        public processRunnable(SSSocket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (!responses.isEmpty()) {
                        byte[] res = responses.take();
                        MainController.process(res);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

}