package Client.Controller.Network;

import java.nio.charset.StandardCharsets;

public class RequestCreator {
    public static long requestID = 0;

    public static byte[] signUp(String username, String password, String repeatPass, String email)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.signUp.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"password\":\"" + password +
                "\",\"confirmPassword\":\"" + repeatPass +
                "\",\"email\":\"" + email + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] logIn(String username, String password)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.logIn.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"password\":\"" + password + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] logOut(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.logOut.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] setting(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.setting.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] profilePage(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.profilePage.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);

    }

    public static byte[] saveSetting(String username, String newUsername, String bio, boolean privacy)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.saveSetting.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"newUsername\":\"" + newUsername +
                "\",\"privacy\":" + privacy +
                ",\"bio\":\"" + bio + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] pageChangePassword(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.pageChangePassword.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] changePassword(String username, String oldPassword, String newPassword, String newPassword2)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.changePassword.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"password\":\"" + oldPassword +
                "\",\"newPassword1\":\"" + newPassword +
                "\",\"newPassword2\":\"" + newPassword2 + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] search(String username, String search)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.search.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"search\":\"" + search + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] othersPage(String username, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.othersPage.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] follow(String username, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.follow.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] unfollow(String username, String otherUsername, String buttonType)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.unfollow.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"otherUsername\":\"" + otherUsername +
                "\",\"buttonType\":\"" + buttonType + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] block(String username, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.block.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] showFollowers(String username, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.showFollowers.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] showFollowing(String username, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.showFollowing.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] showBlocked(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.showBlockeds.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] unblock(String username, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.unblock.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] newPost(String username, String caption)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.newPost.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"caption\":\"" + caption + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] showPosts(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.showPost.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] homePage(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.homePage.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] like(String ownerUsername, String otherUsername, long postID)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.like.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"otherUsername\":\"" + otherUsername +
                "\",\"postID\":" + postID + "}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] unlike(String ownerUsername, String otherUsername, long postID)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.unlike.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"otherUsername\":\"" + otherUsername +
                "\",\"postID\":" + postID + "}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] showNotifications(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.showNotifications.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] message(String ownerUsername, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.message.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] newMessage(String ownerUsername, String otherUsername, String text)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.newMessage.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"text\":\"" + text +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] showDirect(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.showDirect.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] directSearch(String username, String search)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.directSearch.name() +
                "\",\"ownerUsername\":\"" + username +
                "\",\"search\":\"" + search + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] accept(String ownerUsername, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.accept.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] reject(String ownerUsername, String otherUsername)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.reject.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"otherUsername\":\"" + otherUsername + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] showComments(String ownerUsername, String otherUsername, long postID)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.showComments.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"otherUsername\":\"" + otherUsername +
                "\",\"postID\":" + postID + "}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] newComment(String ownerUsername, String otherUsername, long postID, String text)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.newComment.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"otherUsername\":\"" + otherUsername +
                "\",\"text\":\"" + text +
                "\",\"postID\":" + postID + "}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] forgotPassword(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.forgotPassword.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] showLikes(String ownerUsername, long postID)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.showLikes.name() +
                "\",\"ownerUsername\":\"" + ownerUsername +
                "\",\"postID\":" + postID + "}";
        return req.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] newProfilePicture(String username)
    {
        requestID++;
        String req = "{\"requestID\":" + requestID +
                ",\"requestType\":\"" + RequestType.newProfilePicture.name() +
                "\",\"ownerUsername\":\"" + username + "\"}";
        return req.getBytes(StandardCharsets.UTF_8);
    }
}
