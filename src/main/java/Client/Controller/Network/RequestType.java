package Client.Controller.Network;

public enum RequestType
{

    logIn, signUp, setting, profilePage, saveSetting, search,
    pageChangePassword, changePassword, othersPage, follow,
    unfollow, block, showFollowers, showFollowing, showBlockeds,
    unblock, newPost, showPost, homePage, like, unlike, logOut,
    showNotifications, message, newMessage, showDirect, directSearch,
    accept, reject, showComments, newComment, forgotPassword,
    showLikes, newProfilePicture,

}
