package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class chatBoxController implements Initializable {
    @FXML
    private Label otherUsername;

    @FXML
    private VBox showChats;

    @FXML
    private JFXTextField textMessage;

    public static Label otherUsernameSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        otherUsernameSet = otherUsername;
        showChats.getChildren().addAll(Account.chatPanes);
    }

    public void sendMessage(ActionEvent actionEvent)
    {
        Account.chatPanes.clear();

        NetworkController.addRequest(RequestCreator.newMessage(Account.username , otherUsername.getText() ,
                textMessage.getText()), RequestCreator.requestID , actionEvent);
    }

    public void back(ActionEvent actionEvent)
    {
        Account.chatPanes.clear();

        if (Account.previousPage.equals("direct"))
        {
            NetworkController.addRequest(RequestCreator.showDirect(Account.username), RequestCreator.requestID, actionEvent);
        }
        else
        {
            NetworkController.addRequest(RequestCreator.othersPage(Account.username, Account.previousPage ),
                    RequestCreator.requestID, actionEvent);
        }
    }
}
