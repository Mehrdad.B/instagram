package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class showNotificationController implements Initializable {
    @FXML
    private VBox notificationBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        notificationBox.getChildren().addAll(Account.notifPanes);
    }

    public void close(MouseEvent mouseEvent) {
        System.exit(0);
    }

    public void goSearch(ActionEvent actionEvent)
    {
        Account.notifPanes.clear();
        try {
            PagesController.openPage("Search");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goAddPost(ActionEvent actionEvent)
    {
        Account.notifPanes.clear();
        try {
            PagesController.openPage("addPost");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goProfile(ActionEvent actionEvent)
    {
        Account.notifPanes.clear();
        NetworkController.addRequest(RequestCreator.profilePage(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void goHomePage(ActionEvent actionEvent)
    {
        Account.notifPanes.clear();
        NetworkController.addRequest(RequestCreator.homePage(Account.username), RequestCreator.requestID, actionEvent);
    }
}
