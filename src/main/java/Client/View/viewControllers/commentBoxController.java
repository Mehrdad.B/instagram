package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class commentBoxController implements Initializable {

    @FXML
    private JFXTextField comment;

    @FXML
    private VBox showComments;

    @FXML
    private Label otherUsername;

    @FXML
    private Label postID;

    public static Label postIDSet , otherUsernameSet;


    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        postIDSet = postID;
        otherUsernameSet = otherUsername;
        showComments.getChildren().addAll(Account.commentPanes);
    }

    public void back(ActionEvent actionEvent)
    {
        Account.commentPanes.clear();
        if (Account.previousPage.equals("home page"))
        {
            Account.panes.clear();
            NetworkController.addRequest(RequestCreator.homePage(Account.username), RequestCreator.requestID, actionEvent);
        }
        else
        {
            NetworkController.addRequest(RequestCreator.showPosts(Account.previousPage) , RequestCreator.requestID , actionEvent);
        }
    }

    public void newComment(ActionEvent actionEvent)
    {
        Account.commentPanes.clear();
        NetworkController.addRequest(RequestCreator.newComment(Account.username , otherUsername.getText() ,
                Long.parseLong(postID.getText()) , comment.getText()), RequestCreator.requestID , actionEvent);
    }
}
