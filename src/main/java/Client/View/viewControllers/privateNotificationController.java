package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class privateNotificationController implements Initializable {
    @FXML
    private Label notification;

    public static  Label notificationSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        notificationSet = notification;
    }

    public void acceptFollow(ActionEvent actionEvent)
    {
        Account.notifPanes.clear();
        String[] strings = notification.getText().split(" ");
        NetworkController.addRequest(RequestCreator.accept(Account.username , strings[0]) ,
                RequestCreator.requestID , actionEvent);
    }

    public void rejectFollow(ActionEvent actionEvent)
    {
        Account.notifPanes.clear();
        String[] strings = notification.getText().split(" ");
        NetworkController.addRequest(RequestCreator.reject(Account.username , strings[0]) ,
                RequestCreator.requestID , actionEvent);
    }
}
