package Client.View.viewControllers;

import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class forgotPasswordController implements Initializable
{

    @FXML
    private JFXTextField Username;

    @FXML
    private Label forgotPasswordAlert;

    public static Label forgotPasswordAlertSet;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        forgotPasswordAlertSet = forgotPasswordAlert;

    }

    public void closeApp(MouseEvent mouseEvent)
    {
        System.exit(0);
    }

    public void minimizeApp(MouseEvent mouseEvent)
    {
        ((Stage) ((Circle) mouseEvent.getSource()).getScene().getWindow()).setIconified(true);
    }

    public void openProfile(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.forgotPassword(Username.getText()) , RequestCreator.requestID , actionEvent);
    }

    public void backToLogin(ActionEvent actionEvent)
    {
        try {
            PagesController.openPage("login");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
