package Client.View.viewControllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class PagesController
{
    private static double xOffset;
    private static double yOffset;
    private static Stage stage;

    public static void closePage(ActionEvent actionEvent)
    {
        Stage newStage = (Stage)((Node) actionEvent.getSource()).getScene().getWindow();
        newStage.close();
    }

    public static void openPage(String name) throws IOException
    {
        stage = new Stage();
        Parent root = FXMLLoader.load(PagesController.class.getResource("/"+ name + ".fxml"));

        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });

        stage.setScene(new Scene(root));

        stage.initStyle(StageStyle.TRANSPARENT);

        stage.show();
    }

    public static Stage getStage()
    {
        return stage;
    }
}
