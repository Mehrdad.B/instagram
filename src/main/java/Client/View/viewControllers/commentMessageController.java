package Client.View.viewControllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class commentMessageController implements Initializable {

    @FXML
    private Text text;

    @FXML
    private Label username;

    public static Label usernameSet;

    public static Text textSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        textSet = text;
        usernameSet = username;
    }
}
