package Client.View.viewControllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import javafx.scene.shape.Circle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class othersPageController implements Initializable {

    @FXML
    private Label username;

    @FXML
    private Label postNumber;

    @FXML
    private Label followersNumber;

    @FXML
    private Label followingNumber;

    @FXML
    private JFXTextArea bio;

    @FXML
    private JFXButton followButton;

    @FXML
    private Label blockLabel;

    @FXML
    private Circle profilePic ;

    public static Label usernameSet , postsNumberSet , followersNumberSet , followingNumberSet, blockLabelSet ;

    public static JFXTextArea bioSet;

    public static JFXButton followButtonSet;

    public static Circle profilePicSet;


    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        usernameSet = username;
        postsNumberSet = postNumber;
        followersNumberSet = followersNumber;
        followingNumberSet = followingNumber;
        bioSet = bio;
        followButtonSet = followButton;
        blockLabelSet = blockLabel;
        profilePicSet = profilePic;
    }

    public void closeApp(MouseEvent mouseEvent) {
        System.exit(0);
    }

    public void blockPerson(ActionEvent actionEvent)
    {
        if (blockLabel.getText().equals("Block")) {
            NetworkController.addRequest(RequestCreator.block(Account.username, username.getText()),
                    RequestCreator.requestID, actionEvent);
        }
        else
        {
            NetworkController.addRequest(RequestCreator.unblock(Account.username, username.getText()),
                    RequestCreator.requestID, actionEvent);
        }
    }

    public void showFollowers(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showFollowers(Account.username,
                username.getText()), RequestCreator.requestID, actionEvent);
    }

    public void showFollowing(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showFollowing(Account.username,
                username.getText()), RequestCreator.requestID, actionEvent);
    }

    public void goSearch(ActionEvent actionEvent)
    {
        try {
            PagesController.openPage("Search");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goProfile(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.profilePage(Account.username),
                RequestCreator.requestID, actionEvent);
    }

    public void follow(ActionEvent actionEvent)
    {
        if(followButton.getText().equals("Follow"))
        {
            NetworkController.addRequest(RequestCreator.follow(Account.username, username.getText()),
                    RequestCreator.requestID, actionEvent);
        }
        else
        {
          NetworkController.addRequest(RequestCreator.unfollow(Account.username, username.getText(),
                  followButton.getText()), RequestCreator.requestID, actionEvent);
        }
    }

    public void showPosts(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showPosts(username.getText()), RequestCreator.requestID, actionEvent);
    }

    public void goAddPost(ActionEvent actionEvent)
    {
        try {
            PagesController.openPage("addPost");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goNotification(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showNotifications(Account.username) ,
                RequestCreator.requestID , actionEvent);
    }

    public void goHomePage(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.homePage(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void message(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.message(Account.username , username.getText()) ,
                RequestCreator.requestID, actionEvent);
    }
}
