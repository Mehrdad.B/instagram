package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ProfilePageController implements Initializable {
    @FXML
    private Label Username;

    @FXML
    private JFXTextArea Bio;

    @FXML
    private Label postsNumber;

    @FXML
    private Label FollowersNumber;

    @FXML
    private Label FollowingNumber;

    @FXML
    private Circle profilePic ;

    public static Label usernameSet , postsNumberSet , followersNumberSet , followingNumberSet ;
    public static Circle profilePicSet ;
    public static JFXTextArea bioSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        usernameSet = Username;
        postsNumberSet = postsNumber;
        followersNumberSet = FollowersNumber;
        followingNumberSet = FollowingNumber;
        bioSet = Bio;
        profilePicSet = profilePic ;
    }


    public void Setting(ActionEvent actionEvent) {
        NetworkController.addRequest(RequestCreator.setting(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void close(MouseEvent mouseEvent) {
        System.exit(0);
    }

    public void showFollowing(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showFollowing(Account.username,
                Account.username), RequestCreator.requestID, actionEvent);
    }

    public void showFollowers(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showFollowers(Account.username,
                Account.username), RequestCreator.requestID, actionEvent);
    }

    public void goSearch(ActionEvent actionEvent) {
        try {
            PagesController.openPage("Search");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goAddPost(ActionEvent actionEvent) {
        try {
            PagesController.openPage("addPost");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showPosts(ActionEvent actionEvent) {
        NetworkController.addRequest(RequestCreator.showPosts(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void goHomePage(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.homePage(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void goNotification(ActionEvent actionEvent) {
        NetworkController.addRequest(RequestCreator.showNotifications(Account.username) ,
                RequestCreator.requestID , actionEvent);
    }

}
