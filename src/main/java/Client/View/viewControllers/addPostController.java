package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ResourceBundle;

public class addPostController implements Initializable {
    @FXML
    private JFXTextArea caption;

    @FXML
    private Label postAlert;

    public static Label postAlertSet;

    public static JFXTextArea  captionSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("image Files"
                , "*.jpg" , "*.png" , "*.jpeg");
        Account.fileChooser.getExtensionFilters().add(filter);
        postAlertSet = postAlert;
        captionSet = caption;
    }

    public void close(MouseEvent mouseEvent) {
        System.exit(0);
    }

    public void openPicture(ActionEvent actionEvent) {

        Account.file = Account.fileChooser.showOpenDialog(null);
        postAlertSet.setText("");
        caption.setText("");

    }

    public void goSearch(ActionEvent actionEvent) {
        Account.file = null;
        caption.setText("");
        try {
            PagesController.openPage("Search");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goProfile(ActionEvent actionEvent) {
        Account.file = null;
        caption.setText("");
        NetworkController.addRequest(RequestCreator.profilePage(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void savePost(ActionEvent actionEvent)
    {
        try {
            if (Account.file != null)
            {
                if (pictureSizeCheck(Account.file) > 20000)
                {
                    postAlertSet.setText("File size too big! Try again!");
                }
                else
                {
                    byte[] data = Files.readAllBytes(Account.file.toPath());
                    NetworkController.addRequest(RequestCreator.newPost(Account.username,
                            caption.getText()), RequestCreator.requestID, actionEvent);
                }
            }
            else
            {
                postAlert.setText("No file selected !");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private int pictureSizeCheck(File file)
    {
        return (int) file.length();
    }

    public void goAddPost(ActionEvent actionEvent) {
    }

    public void goNotificaation(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showNotifications(Account.username) ,
                RequestCreator.requestID , actionEvent);
    }

    public void goHomePage(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.homePage(Account.username), RequestCreator.requestID, actionEvent);
    }
}
