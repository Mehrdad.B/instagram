package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXPasswordField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class changePasswordController implements Initializable
{
    @FXML
    private JFXPasswordField oldPassword;

    @FXML
    private JFXPasswordField newPassword;

    @FXML
    private JFXPasswordField confirmPassword;

    @FXML
    private Label changePassAlert;

    public static Label changePassAlertSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        changePassAlertSet = changePassAlert;
    }

    public void backToSetting(ActionEvent actionEvent) {
        NetworkController.addRequest(RequestCreator.setting(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void changePassword(ActionEvent actionEvent) {
        NetworkController.addRequest(RequestCreator.changePassword(Account.username,
                oldPassword.getText(), newPassword.getText(), confirmPassword.getText()),
                RequestCreator.requestID, actionEvent);
    }

    public void close(MouseEvent mouseEvent) {
        System.exit(0);
    }
}
