package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class settingController implements Initializable {

    @FXML
    private JFXTextField Username;

    @FXML
    private JFXToggleButton privacy;

    @FXML
    private JFXTextArea Biography;

    @FXML
    private Label settingAlert;

    public static JFXTextField usernameSet;

    public static JFXTextArea biographySet;

    public static JFXToggleButton privacySet;

    public static Label settingAlertSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usernameSet = Username;
        privacySet = privacy;
        biographySet = Biography;
        settingAlertSet = settingAlert;
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("image Files"
                , "*.jpg" , "*.png" , "*.jpeg");
        Account.fileChooser.getExtensionFilters().add(filter);
    }

    public void backProfile(ActionEvent actionEvent) {
        Account.file = null ;
        NetworkController.addRequest(RequestCreator.profilePage(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void saveInf(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.saveSetting(Account.username, Username.getText(),
                Biography.getText(), Account.privacy), RequestCreator.requestID, actionEvent);

        if (Account.file != null)
        {
            if (Account.file != null)
            {
                if (pictureSizeCheck(Account.file) > 20000)
                {
                    settingAlert.setText("File size too big! Try again!");
                }
                else
                {
                    NetworkController.addRequest(RequestCreator.newProfilePicture(Account.username),
                            RequestCreator.requestID, actionEvent);
                }
            }
        }
    }

    private int pictureSizeCheck(File file)
    {
        return (int) file.length();
    }

    @FXML
    void changePrivacy(ActionEvent event) {

        if (privacy.getText().equals("private"))
        {
            privacy.setText("public");
            Account.privacy = false;
        }
        else
        {
            privacy.setText("private");
            Account.privacy = true;
        }
    }


    public void openChangePassword(ActionEvent actionEvent)
    {
        Account.file = null ;
        NetworkController.addRequest(RequestCreator.pageChangePassword(Account.username),
                RequestCreator.requestID, actionEvent);
    }

    public void close(MouseEvent mouseEvent)
    {
        Account.file = null ;
        System.exit(0);
    }

    public void logOut(ActionEvent actionEvent) {
        Account.file = null ;
        NetworkController.addRequest(RequestCreator.logOut(Account.username) , RequestCreator.requestID , actionEvent);
        Account.username = "";
        try {
            PagesController.openPage("login");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void blocks(ActionEvent actionEvent)
    {
        Account.file = null ;
        NetworkController.addRequest(RequestCreator.showBlocked(Account.username),
                RequestCreator.requestID, actionEvent);
    }

    public void changeProfilePhoto(ActionEvent actionEvent) {

        Account.file = Account.fileChooser.showOpenDialog(null) ;

        if (Account.file != null)
        {
            if (pictureSizeCheck(Account.file) > 20000) {
                settingAlert.setText("File size too big! Try again!");
            }
        }

    }
}
