package Client.View.viewControllers;

import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class loginController implements Initializable {

    @FXML
    private Circle close;

    @FXML
    private Circle minimize;

    @FXML
    void closeApp(MouseEvent event) {
        System.exit(0);

    }

    @FXML
    private JFXTextField Username;

    @FXML
    private JFXPasswordField Password;

    @FXML
    private Label LoginAlert;

    public static Label LoginAlertSet;

    @FXML
    void minimizeApp(MouseEvent event) {
        ((Stage) ((Circle) event.getSource()).getScene().getWindow()).setIconified(true);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        LoginAlertSet = LoginAlert   ;
    }

    public void goPageSignup(ActionEvent actionEvent) {
        try {
            PagesController.openPage("signup");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openProfile(ActionEvent actionEvent) {
        NetworkController.addRequest(RequestCreator.logIn(Username.getText(),
                Password.getText()), RequestCreator.requestID, actionEvent);
    }

    public void sendPasEmail(ActionEvent actionEvent) {
        try {
            PagesController.openPage("forgotPassword");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
