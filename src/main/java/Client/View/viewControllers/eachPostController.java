package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXTextArea;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;

import java.net.URL;
import java.util.ResourceBundle;

public class eachPostController implements Initializable {


    @FXML
    private ImageView imageView;

    @FXML
    private Label userName;

    @FXML
    private JFXTextArea caption;

    @FXML
    private Label likesNumber;

    @FXML
    private Label postID;

    @FXML
    private FontAwesomeIconView like;

    public static ImageView imageViewSet ;
    public static Label userNameSet , likesNumberSet , postIDSet;
    public static FontAwesomeIconView likeSet;
    public static JFXTextArea captionSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imageViewSet = imageView;
        userNameSet = userName;
        likesNumberSet = likesNumber;
        captionSet = caption;
        postIDSet = postID;
        likeSet = like;
    }

    public void likePost(ActionEvent actionEvent)
    {
        if (like.getFill().equals(Paint.valueOf("#fa1304")))
        {
            like.setFill(Paint.valueOf("#030303"));
            int like = Integer.parseInt(likesNumber.getText());
            likesNumber.setText(String.valueOf(like - 1));

            NetworkController.addRequest(RequestCreator.unlike(Account.username, userName.getText(),
                    Long.parseLong(postID.getText())), RequestCreator.requestID, actionEvent);
        }
        else
        {
            like.setFill(Paint.valueOf("#fa1304"));
            int like = Integer.parseInt(likesNumber.getText());
            likesNumber.setText(String.valueOf(like + 1));

            NetworkController.addRequest(RequestCreator.like(Account.username, userName.getText(),
                    Long.parseLong(postID.getText())), RequestCreator.requestID, actionEvent);
        }
    }

    public void showComments(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showComments(Account.username , userName.getText() ,
                Long.parseLong(postID.getText())) , RequestCreator.requestID , actionEvent);
    }

    public void viewComments(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showComments(Account.username , userName.getText() ,
                Long.parseLong(postID.getText())) , RequestCreator.requestID , actionEvent);
    }

    public void showLikes(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showLikes(Account.username , Long.parseLong(postID.getText())) ,
                RequestCreator.requestID , actionEvent);
    }
}
