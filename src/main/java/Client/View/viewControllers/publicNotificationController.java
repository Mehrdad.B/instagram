package Client.View.viewControllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class publicNotificationController implements Initializable {
    @FXML
    private Label notification;

    public static Label notificationSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        notificationSet = notification;
    }
}
