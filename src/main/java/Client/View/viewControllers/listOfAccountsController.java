package Client.View.viewControllers;

import com.jfoenix.controls.JFXListView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class listOfAccountsController implements Initializable {

    @FXML
    private JFXListView showList;

    @FXML
    private Label Username;

    @FXML
    private Label accountSubject;

    public static Label usernameSet, accountSubjectSet;

    public static JFXListView showListSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usernameSet = Username;
        accountSubjectSet = accountSubject;
        showListSet = showList;
    }

    public void closeApp(MouseEvent mouseEvent) {
        System.exit(0);
    }


    public void backToSearch(ActionEvent actionEvent)
    {
        Account.panes.clear();
        switch (Account.previousPage) {
            case "profile page": {
                NetworkController.addRequest(RequestCreator.profilePage(Account.username),
                        RequestCreator.requestID, actionEvent);
                break;
            }
            case "setting": {
                NetworkController.addRequest(RequestCreator.setting(Account.username),
                        RequestCreator.requestID, actionEvent);
                break;
            }
            case "search": {
                try {
                    PagesController.openPage("Search");
                    PagesController.closePage(actionEvent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "home page" :
            {
                NetworkController.addRequest(RequestCreator.homePage(Account.username), RequestCreator.requestID, actionEvent);
                break;
            }
            default: {
                NetworkController.addRequest(RequestCreator.othersPage(Account.username, Account.previousPage),
                        RequestCreator.requestID, actionEvent);
                break;
            }
        }
    }

    public void showOthersPage(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.othersPage(Account.username, (String) showList.getSelectionModel().getSelectedItem() )
                , RequestCreator.requestID, actionEvent);
    }
}
