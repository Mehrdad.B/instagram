package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class directMessageController implements Initializable {
    @FXML
    private JFXTextField search;

    @FXML
    private JFXListView chatList;

    @FXML
    private Label username;

    public static Label usernameSet;

    public static JFXListView chatListSet;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        usernameSet = username;
        chatListSet = chatList;
    }

    public void showSearchResult(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.directSearch(Account.username,
                search.getText()), RequestCreator.requestID, actionEvent);
    }

    public void showChat(ActionEvent actionEvent)
    {
        Account.previousPage = "direct";
        NetworkController.addRequest(RequestCreator.message(Account.username , (String)chatList.getSelectionModel().getSelectedItem()),
                RequestCreator.requestID, actionEvent);
    }

    public void backHomePage(ActionEvent actionEvent)
    {
        Account.panes.clear();
        NetworkController.addRequest(RequestCreator.homePage(Account.username), RequestCreator.requestID, actionEvent);
    }
}
