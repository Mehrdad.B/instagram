package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class homePageController implements Initializable {

    @FXML
    private VBox homePost;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        homePost.getChildren().addAll(Account.panes);
    }

    public void close(MouseEvent mouseEvent)
    {
        System.exit(0);
    }

    public void goSearch(ActionEvent actionEvent)
    {
        Account.panes.clear();
        try {
            PagesController.openPage("Search");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goAddPost(ActionEvent actionEvent)
    {
        Account.panes.clear();
        try {
            PagesController.openPage("addPost");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goNotification(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showNotifications(Account.username) ,
                RequestCreator.requestID , actionEvent);
    }

    public void goProfile(ActionEvent actionEvent)
    {
        Account.panes.clear();
        NetworkController.addRequest(RequestCreator.profilePage(Account.username),
                RequestCreator.requestID, actionEvent);
    }

    public void goDirect(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showDirect(Account.username), RequestCreator.requestID, actionEvent);
    }
}
