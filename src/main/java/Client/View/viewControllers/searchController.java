package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class searchController implements Initializable {

    @FXML
    private JFXTextField search;

    @FXML
    private Label searchResult;

    public static Label searchAlertSet;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        searchAlertSet = searchResult;
    }

    public void goProfile(ActionEvent actionEvent) {
        NetworkController.addRequest(RequestCreator.profilePage(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void close(MouseEvent mouseEvent) {
        System.exit(0);
    }

    public void showSearchResult(ActionEvent actionEvent) {
        NetworkController.addRequest(RequestCreator.search(Account.username,
                search.getText()), RequestCreator.requestID, actionEvent);
    }

    public void goHomePage(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.homePage(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void goAddPost(ActionEvent actionEvent)
    {
        try {
            PagesController.openPage("addPost");
            PagesController.closePage(actionEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goNotifications(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showNotifications(Account.username) ,
                RequestCreator.requestID , actionEvent);
    }
}
