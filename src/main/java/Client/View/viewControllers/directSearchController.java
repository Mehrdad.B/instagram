package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXListView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class directSearchController implements Initializable
{
    @FXML
    private JFXListView showList;

    @FXML
    private Label Username;

    public static Label usernameSet;

    public static JFXListView showListSet;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        usernameSet = Username;
        showListSet = showList;
    }

    public void closeApp(MouseEvent mouseEvent)
    {
        System.exit(0);
    }

    public void backToDirect(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.showDirect(Account.username), RequestCreator.requestID, actionEvent);
    }

    public void showChat(ActionEvent actionEvent)
    {
        NetworkController.addRequest(RequestCreator.message(Account.username , (String)showList.getSelectionModel().getSelectedItem()),
                RequestCreator.requestID, actionEvent);
    }
}
