package Client.View.viewControllers;

import Client.Controller.Data.Account;
import Client.Controller.Network.NetworkController;
import Client.Controller.Network.RequestCreator;
import com.jfoenix.controls.JFXScrollPane;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class postsController implements Initializable {

    @FXML
    private JFXScrollPane scrollPosts;

    @FXML
    private VBox postBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        postBox.getChildren().addAll(Account.panes);
    }

    public void backAccount(ActionEvent actionEvent)
    {
        Account.panes.clear();
        if (Account.previousPage.equals("profile page"))
        {
            NetworkController.addRequest(RequestCreator.profilePage(Account.username),
                    RequestCreator.requestID, actionEvent);
        }
        else
        {
            NetworkController.addRequest(RequestCreator.othersPage(Account.username, Account.previousPage),
                    RequestCreator.requestID, actionEvent);
        }
    }

    public void close(MouseEvent mouseEvent) {
        System.exit(0);
    }
}
