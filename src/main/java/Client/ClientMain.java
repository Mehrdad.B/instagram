package Client;

import Client.Controller.Network.NetworkController;
import Client.View.viewControllers.PagesController;
import javafx.application.Application;
import javafx.stage.Stage;

public class ClientMain extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        NetworkController networkController = new NetworkController();
        PagesController.openPage("login");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
