package server.JsonHandler;

import javafx.beans.property.adapter.JavaBeanBooleanPropertyBuilder;

public enum RequestDetails {
    requestID , requestType , email , bio , password , newPassword1 ,
    newPassword2 , ownerUsername , otherUsername , successful ,
    response , followersNumber , followingNumber , postsNumber ,
    privacy , search , list, visible , blockLabel, followLabel ,
    caption , likeNumber , postID , color , notification , text ,
    hasPicture ,
}
