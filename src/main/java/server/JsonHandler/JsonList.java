package server.JsonHandler;

import com.google.gson.Gson;
import com.mongodb.DBObject;
import server.Manager.DataBase.FieldDetails;

import java.util.ArrayList;
import java.util.List;

public class JsonList {

    private static Gson gson = new Gson() ;

    public static String getJson (List<DBObject> list )
    {
        ArrayList arrayList = new ArrayList() ;
        for ( DBObject dbObject : list) {

            arrayList.add(String.valueOf(dbObject.get(FieldDetails.username.name()))) ;

        }

        return gson.toJson(arrayList) ;
    }

    public static String getJsonArray (List<String> list )
    {
        return gson.toJson(list) ;
    }
}
