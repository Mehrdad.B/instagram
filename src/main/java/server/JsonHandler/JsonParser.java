package server.JsonHandler;


import com.google.gson.Gson;
import server.Manager.MainManager;


public class JsonParser extends Parser{

    private Gson gson = new Gson() ;
   @Override
   public void handle (byte[] bytes)
   {
       String jsonRequest = new String(bytes) ;
       MainManager.setRequest(gson.fromJson(jsonRequest,Request.class) );
   }



}
