package server.JsonHandler;


public class Request {

    private long requestID ;
    private String requestType ;
    private String email ;
    private String bio;
    private String password ;
    private String confirmPassword;
    private String newPassword1 ;
    private String newPassword2 ;
    private String ownerUsername ;
    private String otherUsername ;
    private String newUsername ;
    private String search;
    private boolean privacy ;
    private String buttonType ;
    private String caption;
    private long postID;
    private String text;

    public long getRequestID() {
        return requestID;
    }

    public String getRequestType() {
        return requestType;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public String getNewPassword1() {
        return newPassword1;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public String getBio() {
        return bio;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public String getOtherUsername() {
        return otherUsername;
    }

    public String getNewUsername() {
        return newUsername;
    }

    public boolean getPrivacy() {
        return privacy;
    }

    public String getSearch() {
        return search;
    }

    public String getButtonType() {
        return buttonType;
    }

    public String getCaption() {
        return caption;
    }

    public long getPostID() {
        return postID;
    }

    public String getText() {
        return text;
    }
}
