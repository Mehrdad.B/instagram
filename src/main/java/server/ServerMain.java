package server;

import Client.Controller.ConnectionValues;
import server.Manager.Network.NetworkManager;
import server.Manager.Network.SSSocket ;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ServerMain {
    public static Map<String , NetworkManager> onlineUsers = new HashMap<>();

    public static void main(String[] args) throws IOException {

        System.out.println("Server is running !");

//        DataBaseManager dataBaseManager = new DataBaseManager("oonstagramTest") ;
//        dataBaseManager.deleteAll();

        ServerSocket server = new ServerSocket(ConnectionValues.PORT_NUMBER) ;

        while (true)
        {
            Socket socket = server.accept();
            SSSocket ssSocket = new SSSocket(socket) ;
            System.out.println("someone connected !");
            NetworkManager networkManager = new NetworkManager(ssSocket) ;

            Thread connection = new Thread(networkManager) ;
            connection.start();
        }
    }
}
