package server.Manager.Network;


import server.Manager.MainManager;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class NetworkManager implements Runnable {

    private BlockingDeque<byte[]> requests ;
    private BlockingDeque<byte[]> responses ;
    private Thread send ;
    private Thread get ;
    private SSSocket socket ;


    public NetworkManager() throws UnknownHostException {
    }

    public NetworkManager(SSSocket socket) throws UnknownHostException {

        this.socket = socket;
        send = new Thread(new SendRunnable(socket));
        get = new Thread(new GetRunnable(socket));
        requests = new LinkedBlockingDeque<>();
        responses = new LinkedBlockingDeque<>();
        MainManager.setParser(MainManager.getJsonParser());

    }


    public void addResponse (String response)
    {
        try {
            System.out.println("response : " + response);
            responses.put(response.getBytes(StandardCharsets.UTF_8));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void addResponsebyte (byte[] response)
    {
        try {
            responses.put(response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        get.start();
        send.start();

        while (true)
        {
            try {
                byte[] req = requests.take()  ;
                MainManager.main(req , this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private class SendRunnable implements Runnable {

        SSSocket socket ;

        public SendRunnable(SSSocket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            while (true)
            {
                try {
                    if (!responses.isEmpty())
                    {
                        byte[] data = responses.take() ;
                        data = DataManager.encrypt(data) ;
                        socket.sendMessage(data);
                    }
                } catch ( Exception e) {
                    e.printStackTrace();

                }
            }

        }
    }


    private class GetRunnable implements Runnable{
        SSSocket socket ;

        public GetRunnable(SSSocket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            while (true)
            {

                try {
                    byte[] req = (socket.readMessage());
                    req = DataManager.decrypt(req);
                    requests.put(req);
                } catch (Exception e) {
                    e.printStackTrace();
                }



            }

        }
    }
}
