package server.Manager;

import Client.Model.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.JsonHandler.JsonList;
import server.JsonHandler.Request;
import server.Manager.Requests.SettingDetails.*;
import server.ServerMain;
import server.Manager.DataBase.DataBaseManager;
import server.Manager.DataBase.FieldDetails;
import server.Manager.DataBase.SendEmail;
import server.Manager.Network.NetworkManager;
import server.Manager.Notifications.*;
import server.Manager.Requests.DirectDetails.Message;
import server.Manager.Requests.DirectDetails.NewMessage;
import server.Manager.Requests.DirectDetails.ShowDirect;
import server.Manager.Requests.LoginDetails.ForgotPassword;
import server.Manager.Requests.LoginDetails.HeartBeat;
import server.Manager.Requests.LoginDetails.LogIn;
import server.Manager.Requests.LoginDetails.SignUp;
import server.Manager.Requests.OonstagramDetails.*;
import server.Manager.Requests.PostDetails.*;
import server.Manager.Requests.ProfileDetails.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Process {


    public static Request request;
    public static DataBaseManager mongo;
    public static String collUsers = "Users", collFollowing = "Following",
            collFollowers = "Followers", collBlocks = "Blocks",
            collRequested = "Requested", collRequesting = "Requesting",
            collPost = "Post", collLike = "Like", collNotif = "Notif",
            collDirect = "Direct", collChat = "Chat", collComment = "Comment";

    static {
        try {
            mongo = new DataBaseManager("Oonstagram");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public static boolean isOnline(String username) {
        return ServerMain.onlineUsers.containsKey(username);
    }

    public static void signUp(NetworkManager networkManager) {
        SignUp signUp = new SignUp(request.getRequestID(), request.getRequestType(), request.getOwnerUsername(),
                request.getEmail(), request.getPassword(),
                request.getConfirmPassword());

        String responseString = signUp.check(mongo.isExist(collUsers, signUp.getOwnerUsername()));

        if (signUp.isValid()) {
            User user = new User(request.getOwnerUsername(), request.getPassword(), request.getEmail());
            mongo.insert(collUsers, user.getDbObject());
        }

        networkManager.addResponse(signUp.response(responseString));

    }

    public static void logIn(NetworkManager networkManager) {

        LogIn logIn = new LogIn(request.getRequestID(), request.getRequestType(), request.getOwnerUsername(),
                request.getPassword());

        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), logIn.getOwnerUsername());
        String responseString;
        try {
            responseString = logIn.check(mongo.isExist(collUsers, request.getOwnerUsername()),
                    data.get(FieldDetails.password.name()).equals(request.getPassword()));
        } catch (NullPointerException e) {
            responseString = logIn.check(mongo.isExist(collUsers, request.getOwnerUsername()), false);
        }
        networkManager.addResponse(logIn.response(responseString));


        if (logIn.isValid()) {
            ServerMain.onlineUsers.put(request.getOwnerUsername(), networkManager);
        }
    }

    public static void logOut(NetworkManager networkManager) {
        ServerMain.onlineUsers.remove(request.getOwnerUsername());
    }

    public static void follow(NetworkManager networkManager) {

        Follow follow = new Follow(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        DBObject dataOther = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOtherUsername());
        DBObject dataOwner = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOwnerUsername());

        boolean privacy = Boolean.parseBoolean(String.valueOf(dataOther.get(FieldDetails.privacy.name())));
        boolean block = mongo.isExist(collBlocks + request.getOtherUsername(), request.getOwnerUsername());
        block |= mongo.isExist(collBlocks + request.getOwnerUsername(), request.getOtherUsername());

        String responseString = follow.check(block, privacy);

        if (follow.isValid()) {

            DBObject data1 = new BasicDBObject().append(FieldDetails.username.name(), request.getOtherUsername());
            mongo.insert(collFollowing + request.getOwnerUsername(), data1);

            int followingNum = Integer.parseInt(String.valueOf(dataOwner.get(FieldDetails.followingNumber.name())));
            followingNum++;
            mongo.updateAndAdd(collUsers, dataOwner, FieldDetails.followingNumber.name(), String.valueOf(followingNum));

            DBObject data2 = new BasicDBObject().append(FieldDetails.username.name(), request.getOwnerUsername());
            mongo.insert(collFollowers + request.getOtherUsername(), data2);

            int followersNum = Integer.parseInt(String.valueOf(dataOther.get(FieldDetails.followersNumber.name())));
            followersNum++;
            mongo.updateAndAdd(collUsers, dataOther, FieldDetails.followersNumber.name(), String.valueOf(followersNum));


            FollowNotif followNotif = new FollowNotif(request.getOwnerUsername(), request.getOtherUsername());
            mongo.insert(collNotif + request.getOtherUsername(), followNotif.getDBObject());

            if (isOnline(request.getOtherUsername())) {
                ServerMain.onlineUsers.get(request.getOtherUsername()).addResponse(followNotif.notification(notifList()));
            }


        } else if (!block) {
            DBObject data1 = new BasicDBObject().append(FieldDetails.username.name(), request.getOtherUsername());
            mongo.insert(collRequesting + request.getOwnerUsername(), data1);

            DBObject data2 = new BasicDBObject().append(FieldDetails.username.name(), request.getOwnerUsername());
            mongo.insert(collRequested + request.getOtherUsername(), data2);


            FollowRequestNotif followRequestNotif = new FollowRequestNotif(request.getOwnerUsername(), request.getOtherUsername());
            mongo.insert(collNotif + request.getOtherUsername(), followRequestNotif.getDBObject());

            if (isOnline(request.getOtherUsername())) {
                ServerMain.onlineUsers.get(request.getOtherUsername()).addResponse(followRequestNotif.notification(notifList()));
            }

        }

        networkManager.addResponse(follow.response(responseString));

    }

    public static void unfollow(NetworkManager networkManager) {
        Unfollow unfollow = new Unfollow(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        DBObject dataOther = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOtherUsername());
        DBObject dataOwner = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOwnerUsername());

        if (request.getButtonType().equals("Unfollow")) {
            DBObject data1 = new BasicDBObject().append(FieldDetails.username.name(), request.getOtherUsername());
            mongo.drop(collFollowing + request.getOwnerUsername(), data1);

            int followingNum = Integer.parseInt(String.valueOf(dataOwner.get(FieldDetails.followingNumber.name())));
            followingNum--;
            if (followingNum < 0) followingNum = 0;
            mongo.updateAndAdd(collUsers, dataOwner, FieldDetails.followingNumber.name(), String.valueOf(followingNum));


            DBObject data2 = new BasicDBObject().append(FieldDetails.username.name(), request.getOwnerUsername());
            mongo.drop(collFollowers + request.getOtherUsername(), data2);

            int followersNum = Integer.parseInt(String.valueOf(dataOther.get(FieldDetails.followersNumber.name())));
            followersNum--;
            if (followersNum < 0) followersNum = 0;
            mongo.updateAndAdd(collUsers, dataOther, FieldDetails.followersNumber.name(), String.valueOf(followersNum));

        } else {
            DBObject data1 = new BasicDBObject().append(FieldDetails.username.name(), request.getOtherUsername());
            mongo.drop(collRequesting + request.getOwnerUsername(), data1);

            DBObject data2 = new BasicDBObject().append(FieldDetails.username.name(), request.getOwnerUsername());
            mongo.drop(collRequested + request.getOtherUsername(), data2);


            FollowRequestNotif followRequestNotif = new FollowRequestNotif(request.getOwnerUsername(), request.getOtherUsername());
            mongo.drop(collNotif + request.getOtherUsername(), followRequestNotif.getDBObject());

        }
        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOtherUsername());
        networkManager.addResponse(unfollow.response(Boolean.parseBoolean(String.valueOf(data.get(FieldDetails.privacy.name())))));

    }

    public static void accept(NetworkManager networkManager) {
        Accept accept = new Accept(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        DBObject dataOther = mongo.findByValue(collRequested + request.getOwnerUsername(),
                FieldDetails.username.name(), request.getOtherUsername());
        mongo.drop(collRequested + request.getOwnerUsername(), dataOther);
        mongo.insert(collFollowers + request.getOwnerUsername(), dataOther);

        DBObject dataOwner = mongo.findByValue(collRequesting + request.getOtherUsername(),
                FieldDetails.username.name(), request.getOwnerUsername());
        mongo.drop(collRequesting + request.getOtherUsername(), dataOwner);
        mongo.insert(collFollowing + request.getOtherUsername(), dataOwner);

        FollowRequestNotif followRequestNotif = new FollowRequestNotif(request.getOtherUsername(), request.getOwnerUsername());
        mongo.drop(collNotif + request.getOwnerUsername(), followRequestNotif.getDBObject());
        FollowNotif followNotif = new FollowNotif(request.getOtherUsername(), request.getOwnerUsername());
        mongo.insert(collNotif + request.getOwnerUsername(), followNotif.getDBObject());

        dataOther = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOtherUsername());
        dataOwner = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOwnerUsername());

        int followingNum = Integer.parseInt(String.valueOf(dataOther.get(FieldDetails.followingNumber.name())));
        followingNum++;
        mongo.updateAndAdd(collUsers, dataOther, FieldDetails.followingNumber.name(), String.valueOf(followingNum));

        int followersNum = Integer.parseInt(String.valueOf(dataOwner.get(FieldDetails.followersNumber.name())));
        followersNum++;
        mongo.updateAndAdd(collUsers, dataOwner, FieldDetails.followersNumber.name(), String.valueOf(followersNum));

        showNotifications(networkManager);
    }

    public static void reject(NetworkManager networkManager) {
        Reject reject = new Reject(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        DBObject dataOther = mongo.findByValue(collRequested + request.getOwnerUsername(),
                FieldDetails.username.name(), request.getOtherUsername());
        mongo.drop(collRequested + request.getOwnerUsername(), dataOther);

        DBObject dataOwner = mongo.findByValue(collRequesting + request.getOtherUsername(),
                FieldDetails.username.name(), request.getOwnerUsername());
        mongo.drop(collRequesting + request.getOtherUsername(), dataOwner);

        FollowRequestNotif followRequestNotif = new FollowRequestNotif(request.getOtherUsername(), request.getOwnerUsername());
        mongo.drop(collNotif + request.getOwnerUsername(), followRequestNotif.getDBObject());

        showNotifications(networkManager);
    }

    public static void block(NetworkManager networkManager) {
        Block block = new Block(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        DBObject dataOther = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOtherUsername());
        DBObject dataOwner = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOwnerUsername());

        mongo.insert(collBlocks + request.getOwnerUsername(), dataOther);

        if (mongo.isExist(collFollowing + request.getOwnerUsername(), request.getOtherUsername())) {

            DBObject data1 = new BasicDBObject().append(FieldDetails.username.name(), request.getOtherUsername());
            mongo.drop(collFollowing + request.getOwnerUsername(), data1);
            DBObject data2 = new BasicDBObject().append(FieldDetails.username.name(), request.getOwnerUsername());
            mongo.drop(collFollowers + request.getOtherUsername(), data2);

            int followingNum = Integer.parseInt(String.valueOf(dataOwner.get(FieldDetails.followingNumber.name())));
            followingNum--;
            if (followingNum < 0) followingNum = 0;
            mongo.updateAndAdd(collUsers, dataOwner, FieldDetails.followingNumber.name(), String.valueOf(followingNum));

            int followersNum = Integer.parseInt(String.valueOf(dataOther.get(FieldDetails.followersNumber.name())));
            followersNum--;
            if (followersNum < 0) followersNum = 0;
            mongo.updateAndAdd(collUsers, dataOther, FieldDetails.followersNumber.name(), String.valueOf(followersNum));
        }

        if (mongo.isExist(collFollowing + request.getOtherUsername(), request.getOwnerUsername())) {

            DBObject data1 = new BasicDBObject().append(FieldDetails.username.name(), request.getOtherUsername());
            mongo.drop(collFollowers + request.getOwnerUsername(), data1);
            DBObject data2 = new BasicDBObject().append(FieldDetails.username.name(), request.getOwnerUsername());
            mongo.drop(collFollowing + request.getOtherUsername(), data2);

            int followingNum = Integer.parseInt(String.valueOf(dataOther.get(FieldDetails.followingNumber.name())));
            followingNum--;
            if (followingNum < 0) followingNum = 0;
            mongo.updateAndAdd(collUsers, dataOther, FieldDetails.followingNumber.name(), String.valueOf(followingNum));

            int followersNum = Integer.parseInt(String.valueOf(dataOwner.get(FieldDetails.followersNumber.name())));
            followersNum--;
            if (followersNum < 0) followersNum = 0;
            mongo.updateAndAdd(collUsers, dataOwner, FieldDetails.followersNumber.name(), String.valueOf(followersNum));
        }

        networkManager.addResponse(block.response());
    }

    public static void unblock(NetworkManager networkManager) {
        Unblock unblock = new Unblock(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        DBObject data = new BasicDBObject().append(FieldDetails.username.name(), request.getOtherUsername());
        mongo.drop(collBlocks + request.getOwnerUsername(), data);

        networkManager.addResponse(unblock.response());
    }

    public static void profilePage(NetworkManager networkManager) {
        ProfilePage profilePage = new ProfilePage(request.getRequestID(), request.getRequestType(), request.getOwnerUsername());
        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), profilePage.getOwnerUsername());

        Object obj = data.get(FieldDetails.picture.name());
        byte[] picture = (byte[]) obj;
        profilePage.setValid(picture != null);

        networkManager.addResponse(profilePage.response((String) data.get(FieldDetails.bio.name()),
                Integer.parseInt(String.valueOf(data.get(FieldDetails.followersNumber.name()))),
                Integer.parseInt(String.valueOf(data.get(FieldDetails.followingNumber.name()))),
                Integer.parseInt(String.valueOf(data.get(FieldDetails.postsNumber.name())))));

        if (picture != null) {
            networkManager.addResponsebyte(picture);
        }
    }

    public static void othersPage(NetworkManager networkManager) {

        OthersPage othersPage = new OthersPage(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOtherUsername());

        boolean privacy = Boolean.parseBoolean((String.valueOf(data.get(FieldDetails.privacy.name()))));
        boolean follow = mongo.isExist(collFollowing + request.getOwnerUsername(), request.getOtherUsername());
        boolean blockOwner = mongo.isExist(collBlocks + request.getOwnerUsername(), request.getOtherUsername());
        boolean blockOther = mongo.isExist(collBlocks + request.getOtherUsername(), request.getOwnerUsername());
        boolean requested = mongo.isExist(collRequesting + request.getOwnerUsername(), request.getOtherUsername());

        othersPage.check(privacy, follow, blockOwner, blockOther, requested);

        Object obj = data.get(FieldDetails.picture.name());
        byte[] picture = (byte[]) obj;
        othersPage.setValid(picture != null);

        if (!blockOther) {
            networkManager.addResponse(othersPage.response((String) data.get(FieldDetails.bio.name()),
                    Integer.parseInt(String.valueOf(data.get(FieldDetails.followersNumber.name()))),
                    Integer.parseInt(String.valueOf(data.get(FieldDetails.followingNumber.name()))),
                    Integer.parseInt(String.valueOf(data.get(FieldDetails.postsNumber.name())))));
        } else {
            networkManager.addResponse(othersPage.response((String) data.get(FieldDetails.bio.name()),
                    0, 0, 0));
        }
        if (othersPage.isValid()) {
            networkManager.addResponsebyte(picture);
        }

    }

    public static void search(NetworkManager networkManager) {
        Search search = new Search(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getSearch());

        List<DBObject> list = mongo.findByRegex(collUsers, FieldDetails.username.name(), request.getSearch());

        String responseString = search.check(list.size() != 0, request.getSearch().equals(""));

        networkManager.addResponse(search.response(responseString, JsonList.getJson(list)));
    }

    public static void setting(NetworkManager networkManager) {
        Setting setting = new Setting(request.getRequestID(), request.getRequestType(), request.getOwnerUsername());
        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), setting.getOwnerUsername());

        networkManager.addResponse(setting.response((String) data.get(FieldDetails.bio.name()),
                Boolean.parseBoolean(String.valueOf(data.get(FieldDetails.privacy.name())))));
    }

    public static void saveSetting(NetworkManager networkManager) {
        SaveSetting saveSetting = new SaveSetting(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getNewUsername(), request.getBio(), request.getPrivacy());

        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), saveSetting.getOwnerUsername());

        String responseString = saveSetting.check(mongo.isExist(collUsers, saveSetting.getNewUsername()));

        if (saveSetting.isValid()) {
            mongo.updateAndAdd(collUsers, data, FieldDetails.username.name(), request.getNewUsername());
            data = mongo.findByValue(collUsers, FieldDetails.username.name(), saveSetting.getNewUsername());
            mongo.updateAndAdd(collUsers, data, FieldDetails.bio.name(), request.getBio());
            data = mongo.findByValue(collUsers, FieldDetails.username.name(), saveSetting.getNewUsername());
            mongo.updateAndAdd(collUsers, data, FieldDetails.privacy.name(), Boolean.toString(request.getPrivacy()));
            data = mongo.findByValue(collUsers, FieldDetails.username.name(), saveSetting.getNewUsername());


        } else {
            mongo.updateAndAdd(collUsers, data, FieldDetails.bio.name(), request.getBio());
            data = mongo.findByValue(collUsers, FieldDetails.username.name(), saveSetting.getOwnerUsername());
            mongo.updateAndAdd(collUsers, data, FieldDetails.privacy.name(), Boolean.toString(request.getPrivacy()));
            data = mongo.findByValue(collUsers, FieldDetails.username.name(), saveSetting.getOwnerUsername());
        }

        networkManager.addResponse(saveSetting.response(responseString, (String) data.get(FieldDetails.username.name())));
    }

    public static void changePassword(NetworkManager networkManager) {
        ChangePassword changePassword = new ChangePassword(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getPassword(), request.getNewPassword1(),
                request.getNewPassword2());

        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), changePassword.getOwnerUsername());

        boolean valid = String.valueOf(data.get(FieldDetails.password.name())).equals(request.getPassword());

        String responseString = changePassword.check(valid);

        if (changePassword.isValid()) {
            mongo.updateAndAdd(collUsers, data, FieldDetails.password.name(), request.getNewPassword1());
            data = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOwnerUsername());
        }

        networkManager.addResponse(changePassword.response(responseString));
    }

    public static void showFollowing(NetworkManager networkManager) {
        ShowFollowing following = new ShowFollowing(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        List<DBObject> list = mongo.findAll(collFollowing + request.getOtherUsername());

        networkManager.addResponse(following.response(JsonList.getJson(list)));
    }

    public static void showFollowers(NetworkManager networkManager) {
        ShowFollowers followers = new ShowFollowers(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        List<DBObject> list = mongo.findAll(collFollowers + request.getOtherUsername());

        networkManager.addResponse(followers.response(JsonList.getJson(list)));
    }

    public static void showBlockeds(NetworkManager networkManager) {
        ShowBlockeds blockeds = new ShowBlockeds(request.getRequestID(), request.getRequestType(), request.getOwnerUsername());

        List<DBObject> list = mongo.findAll(collBlocks + request.getOwnerUsername());

        networkManager.addResponse(blockeds.response(JsonList.getJson(list)));
    }

    public static DBObject newPost(NetworkManager networkManager) {
        NewPost newPost = new NewPost(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getCaption());

        long postID = mongo.size(collPost);
        ++postID;
        Post post = new Post(request.getOwnerUsername(), request.getCaption(), postID,
                request.getRequestID(), request.getRequestType());

        mongo.insert(collPost + request.getOwnerUsername(), post.getDBObject());

        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOwnerUsername());

        long postNumber = Long.parseLong(String.valueOf(data.get(FieldDetails.postsNumber.name())));
        postNumber++;
        mongo.updateAndAdd(collUsers, data, FieldDetails.postsNumber.name(), postNumber);

        networkManager.addResponse(newPost.response());

        return post.getDBObject();
    }

    public static void newPostBytes(DBObject post, byte[] imageBytes, NetworkManager networkManager) {
        NewPostBytes newPostBytes = new NewPostBytes(Long.valueOf(String.valueOf(post.get(FieldDetails.requestID.name()))),
                String.valueOf("savePost"),
                String.valueOf(post.get(FieldDetails.username.name())), imageBytes);
        try {
            Files.write(Paths.get("fil2.jpg"), imageBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mongo.updateAndAdd(collPost + newPostBytes.getOwnerUsername(), post,
                FieldDetails.imageBytes.name(), imageBytes);
        mongo.insert(collPost, post);

        networkManager.addResponse(newPostBytes.response());
    }

    public static void showPost(NetworkManager networkManager) {
        ShowPost showPost = new ShowPost(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername());

        long postNumber = mongo.size(collPost + request.getOwnerUsername());

        List<DBObject> postList = mongo.findAll(collPost + request.getOwnerUsername());

        List<DBObject> copyList = new ArrayList<>(postList);
        Collections.reverse(copyList);
        for (DBObject post : copyList) {
            showPost.check(mongo.isExist(collLike + String.valueOf(Long.parseLong
                    (String.valueOf(post.get(FieldDetails.postID.name())))), request.getOwnerUsername()));

            networkManager.addResponse(showPost.response(String.valueOf(post.get(FieldDetails.caption.name())),
                    Long.parseLong(String.valueOf(post.get(FieldDetails.likeNumber.name()))),
                    Long.parseLong(String.valueOf(post.get(FieldDetails.postID.name())))));

            Object obj = post.get(FieldDetails.imageBytes.name());
            byte[] bytes = (byte[]) obj;

            networkManager.addResponsebyte(bytes);
        }


        networkManager.addResponse(showPost.endResponse());
    }

    public static void homePage(NetworkManager networkManager) {
        HomePage homePage = new HomePage(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername());

        List<DBObject> following = mongo.findAll(collFollowing + request.getOwnerUsername());
        List<DBObject> posts = new ArrayList<>();

        for (DBObject user : following) {
            List<DBObject> followingPost = mongo.findAll(collPost +
                    String.valueOf(user.get(FieldDetails.username.name())));
            posts.addAll(followingPost);
        }

        List<DBObject> followingPost = mongo.findAll(collPost + request.getOwnerUsername());
        posts.addAll(followingPost);

        posts = mongo.sortList(posts);
        Collections.reverse(posts);

        int n = Math.min(10, posts.size());

        for (int i = 0; i < n; i++) {
            DBObject post = posts.get(i);
            String caption = String.valueOf(post.get(FieldDetails.caption.name()));
            Long likeNumber = Long.parseLong(String.valueOf(post.get(FieldDetails.likeNumber.name())));
            String username = String.valueOf(post.get(FieldDetails.username.name()));
            long postID = Long.parseLong(String.valueOf(post.get(FieldDetails.postID.name())));

            homePage.check(mongo.isExist(collLike + String.valueOf(postID), request.getOwnerUsername()));

            networkManager.addResponse(homePage.response(caption, likeNumber, username, postID));

            Object obj = post.get(FieldDetails.imageBytes.name());
            byte[] bytes = (byte[]) obj;
            networkManager.addResponsebyte(bytes);
        }

        networkManager.addResponse(homePage.endResponse());
    }

    public static void like(NetworkManager networkManager) {

        Like like = new Like(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername(), request.getPostID());

        DBObject data = mongo.findByValue(collPost + request.getOtherUsername(),
                FieldDetails.postID.name(), request.getPostID());

        long likeNumber = Long.parseLong(String.valueOf(data.get(FieldDetails.likeNumber.name())));

        if (!mongo.isExist(collLike + request.getPostID(), request.getOwnerUsername())) {

            DBObject dbObject = new BasicDBObject().append("username", request.getOwnerUsername());
            mongo.insert(collLike + request.getPostID(), dbObject);
            likeNumber++;
            mongo.updateAndAdd(collPost + request.getOtherUsername(),
                    data, FieldDetails.likeNumber.name(), likeNumber);

            if (!request.getOtherUsername().equals(request.getOwnerUsername())) {
                LikeNotif likeNotif = new LikeNotif(request.getOwnerUsername(), request.getOtherUsername());
                mongo.insert(collNotif + request.getOtherUsername(), likeNotif.getDBObject());

                if (isOnline(request.getOtherUsername()) ){
                    ServerMain.onlineUsers.get(request.getOtherUsername()).addResponse(likeNotif.notification(notifList()));
                }
            }
        }
    }

    public static void unlike(NetworkManager networkManager) {
        Unlike unlike = new Unlike(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername(), request.getPostID());

        DBObject data = mongo.findByValue(collPost + request.getOtherUsername(),
                FieldDetails.postID.name(), request.getPostID());

        long likeNumber = Long.parseLong(String.valueOf(data.get(FieldDetails.likeNumber.name())));

        if (mongo.isExist(collLike + request.getPostID(), request.getOwnerUsername())) {
            DBObject dbObject = new BasicDBObject().append("username", request.getOwnerUsername());
            mongo.drop(collLike + request.getPostID(), dbObject);
            likeNumber--;
            mongo.updateAndAdd(collPost + request.getOtherUsername(),
                    data, FieldDetails.likeNumber.name(), likeNumber);
        }
    }

    public static void message(NetworkManager networkManager) {
        Message message = new Message(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername());

        List<DBObject> list = mongo.findAll(collChat + request.getOwnerUsername() + "_" + request.getOtherUsername());

        List<String> messageList = new ArrayList<>();

        for (DBObject chat : list) {
            messageList.add(String.valueOf(chat.get(FieldDetails.senderUsername.name())));
            messageList.add(String.valueOf(chat.get(FieldDetails.text.name())));
        }

        Collections.reverse(messageList);

        boolean block = mongo.isExist(collBlocks + request.getOwnerUsername(), request.getOtherUsername());
        boolean selfChat = request.getOwnerUsername().equals(request.getOtherUsername());

        networkManager.addResponse(message.response(JsonList.getJsonArray(messageList), message.check(block, selfChat)));
    }

    public static void newMessage(NetworkManager networkManager) {
        NewMessage newMessage = new NewMessage(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername(), request.getText());

        TextMessage message = new TextMessage(request.getOwnerUsername(), request.getOtherUsername(), request.getText());

        mongo.insert(collChat + request.getOwnerUsername() + "_" + request.getOtherUsername(),
                message.getDBObject());
        mongo.insert(collChat + request.getOtherUsername() + "_" + request.getOwnerUsername(),
                message.getDBObject());

        if (!mongo.isExist(collDirect + request.getOwnerUsername(), request.getOtherUsername())) {
            DBObject dataOwner = new BasicDBObject().append(FieldDetails.username.name(), request.getOwnerUsername());
            DBObject dataOther = new BasicDBObject().append(FieldDetails.username.name(), request.getOtherUsername());
            mongo.insert(collDirect + request.getOwnerUsername(), dataOther);
            mongo.insert(collDirect + request.getOtherUsername(), dataOwner);
        }

        List<DBObject> list = mongo.findAll(collChat + request.getOwnerUsername() + "_" + request.getOtherUsername());

        List<String> messageList = new ArrayList<>();

        for (DBObject chat : list) {
            messageList.add(String.valueOf(chat.get(FieldDetails.senderUsername.name())));
            messageList.add(String.valueOf(chat.get(FieldDetails.text.name())));
        }
        Collections.reverse(messageList);

        networkManager.addResponse(newMessage.response((JsonList.getJsonArray(messageList))));

        DirectNotif directNotif = new DirectNotif(request.getOwnerUsername(), request.getOtherUsername(), request.getText());

        if (isOnline(request.getOtherUsername()) && !request.getOwnerUsername().equals(request.getOtherUsername())) {
            ServerMain.onlineUsers.get(request.getOtherUsername()).addResponse(directNotif.notification(JsonList.getJsonArray(messageList)));
        }
    }

    public static void newComment(NetworkManager networkManager) {
        NewComment newComment = new NewComment(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername(), request.getPostID());

        Comment comment = new Comment(request.getOwnerUsername(), request.getText(), request.getPostID());
        mongo.insert(collComment + request.getPostID(), comment.getDBObject());

        showComments(networkManager);

        if (!request.getOwnerUsername().equals(request.getOtherUsername()))
        {
            CommentNotif commentNotif = new CommentNotif(request.getOwnerUsername(), request.getOtherUsername());
            mongo.insert(collNotif + request.getOtherUsername(), commentNotif.getDBObject());

             if (isOnline(request.getOtherUsername())) {
                 ServerMain.onlineUsers.get(request.getOtherUsername()).addResponse(commentNotif.notification(notifList()));
             }
        }
    }

    public static void showNotifications(NetworkManager networkManager) {
        ShowNotifications showNotifications = new ShowNotifications(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername());
        List<DBObject> notifList = mongo.findAll(collNotif + request.getOwnerUsername());
        List<String> list = new ArrayList<>();
        for (DBObject notif : notifList) {
            String text = String.valueOf(notif.get(FieldDetails.text.name()));
            list.add(text);
            String type = String.valueOf(notif.get(FieldDetails.notificationType.name()));
            list.add(type);
        }
        Collections.reverse(list);

        networkManager.addResponse(showNotifications.response(JsonList.getJsonArray(list)));
    }

    public static void showComments(NetworkManager networkManager) {
        ShowComments showComments = new ShowComments(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername(), request.getPostID());

        List<DBObject> comments = mongo.findAll(collComment + request.getPostID());

        List<String> list = new ArrayList<>();

        for (DBObject comment : comments) {
            String writer = String.valueOf(comment.get(FieldDetails.username.name()));
            String text = String.valueOf(comment.get(FieldDetails.text.name()));

            list.add(text);
            list.add(writer);
        }

        Collections.reverse(list);

        networkManager.addResponse(showComments.response(JsonList.getJsonArray(list)));
    }

    public static void showLikes(NetworkManager networkManager) {

        ShowLikes showLikes = new ShowLikes(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), request.getOtherUsername(), request.getPostID());

        List<DBObject> list = mongo.findAll(collLike + request.getPostID());

        networkManager.addResponse(showLikes.response(JsonList.getJson(list)));
    }

    public static void showDirect(NetworkManager networkManager) {
        ShowDirect showDirect = new ShowDirect(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername());

        List<DBObject> list = mongo.findAll(collDirect + request.getOwnerUsername());

        Collections.reverse(list);

        networkManager.addResponse(showDirect.response(JsonList.getJson(list)));
    }

    public static DBObject newProfilePicture(NetworkManager networkManager) {
        NewProfilePicture newProfilePicture = new NewProfilePicture(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername());

        DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOwnerUsername());

        networkManager.addResponse(newProfilePicture.response());

        return data;
    }

    public static void newProfilePictureBytes(DBObject data, byte[] bytes, NetworkManager networkManager) {
        NewProfilePictureBytes newProfilePictureBytes = new NewProfilePictureBytes(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername(), bytes);

        mongo.updateAndAdd(collUsers, data, FieldDetails.picture.name(), bytes);
    }

    public static void forgotPassword(NetworkManager networkManager) {

        ForgotPassword forgotPassword = new ForgotPassword(request.getRequestID(), request.getRequestType(),
                request.getOwnerUsername());

        String responseString = forgotPassword.check(mongo.isExist(collUsers, forgotPassword.getOwnerUsername()));

        if (forgotPassword.isValid()) {
            DBObject data = mongo.findByValue(collUsers, FieldDetails.username.name(), request.getOwnerUsername());

            String email = String.valueOf(data.get(FieldDetails.email.name()));
            String password = String.valueOf(data.get(FieldDetails.password.name()));
            String text = "Your forgotthen password is : " + password + "\n We recommend you to change it ASAP ! ";

            try {
                SendEmail.Send(email, "Oonstagram Password", text);
            } catch (MessagingException e) {
                e.printStackTrace();
            }

            networkManager.addResponse(forgotPassword.response(responseString));
        } else {
            networkManager.addResponse(forgotPassword.response(responseString));
        }
    }

    public static String notifList() {

        List<DBObject> notifList = mongo.findAll(collNotif + request.getOtherUsername());
        List<String> list = new ArrayList<>();
        for (DBObject notif : notifList) {
            String text = String.valueOf(notif.get(FieldDetails.text.name()));
            list.add(text);
            String type = String.valueOf(notif.get(FieldDetails.notificationType.name()));
            list.add(type);
        }
        Collections.reverse(list);
        return JsonList.getJsonArray(list);
    }

    public static void heartBeat(NetworkManager networkManager) {
        HeartBeat heartBeat = new HeartBeat(request.getRequestID(), request.getRequestType(), request.getOwnerUsername());
        networkManager.addResponse(heartBeat.response(true));
    }
}
