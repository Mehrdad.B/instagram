package server.Manager.DataBase;

public enum FieldDetails {
    username, password, email, bio, followersNumber, followingNumber, postsNumber, privacy,
    caption, imageBytes, postID, likeNumber, requestID, requestType, picture, likeNotif,
    followNotif, followRequestNotif, commentNotif, directNotif, notificationType, senderUsername,
    receiverUsername, text,
}
