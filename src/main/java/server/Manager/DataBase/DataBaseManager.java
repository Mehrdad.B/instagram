package server.Manager.DataBase;

import com.mongodb.*;
import java.net.UnknownHostException;
import java.util.Comparator;
import java.util.List;


public class DataBaseManager {
    MongoClient mongoClient;
    DB db;

    public DataBaseManager(String database) throws UnknownHostException
    {
        mongoClient = new MongoClient();
        db = mongoClient.getDB(database) ;
    }
    public void insert (String collectionName , DBObject data )
    {
        db.getCollection(collectionName).insert(data) ;
    }
    public void drop ( String collectionName , DBObject data )
    {
        db.getCollection(collectionName).findAndRemove(data);
    }
    public void updateAndAdd ( String collectionName , DBObject data , String key , Object value   )
    {
        DBObject newData = new BasicDBObject().append("$set" , new BasicDBObject().append(key,value) ) ;
        WriteResult update = db.getCollection(collectionName).update(data, newData);
    }
    public DBObject findByValue ( String collectionName , String key , Object value )
    {
        DBObject dbObject = new BasicDBObject().append(key,value) ;
        return db.getCollection(collectionName).findOne(dbObject);
    }
    public boolean isExist ( String collectionName , Object username )
    {
        DBObject data = findByValue(collectionName , FieldDetails.username.name(), username) ;
        try{
            if (data.get(FieldDetails.password.name()) != null)
            {
                return true;
            }
        }
        catch (NullPointerException e)
        {
            return false;
        }
        return true;
    }

    public List<DBObject> findAll (String collectionName )
    {
        return db.getCollection(collectionName).find().toArray();
    }

    public List<DBObject> findByRegex (String collectionName, String field, String regex)
    {
        DBObject reg = new BasicDBObject().append("$regex", regex);
        DBObject dbObject = new BasicDBObject().append(field, reg);
        return db.getCollection(collectionName).find(dbObject).toArray();
    }
    public long size(String collectionName)
    {
        return db.getCollection(collectionName).count() ;
    }

    public List<DBObject>  sortByValue (String collectionName , String key  )
    {
        DBObject data = new BasicDBObject().append(key , -1 ) ;
       return db.getCollection(collectionName).find().sort(data).toArray() ;
    }

    public List<DBObject> sortList ( List<DBObject> list)
    {
         list.sort(new Compratorr());
         return list ;
    }

    public static class Compratorr implements Comparator<DBObject>
    {

        @Override
        public int compare(DBObject o1, DBObject o2)
        {
            Long id1 = Long.parseLong(String.valueOf(o1.get(FieldDetails.postID.name())));
            Long id2 = Long.parseLong(String.valueOf(o2.get(FieldDetails.postID.name())));


            if (id1 > id2)
            {
                return 1;
            }
            else if (id1 < id2)
            {
                return -1;
            }
            else
                return 0;
        }
    }
    public void deleteCollection ( String collection )
    {
        db.getCollection(collection).drop();
    }
    public void deleteAll ()
    {
        db.dropDatabase();
    }

}