package server.Manager;

import com.google.gson.Gson;
import com.mongodb.DBObject;
import server.JsonHandler.ByteParser;
import server.JsonHandler.JsonParser;
import server.JsonHandler.Parser;
import server.JsonHandler.Request;
import server.Manager.Network.NetworkManager;

import java.net.UnknownHostException;


public class MainManager {


    private static JsonParser jsonParser = new JsonParser();
    private static ByteParser byteParser = new ByteParser();
    private static Parser parser;
    private static Request request;
    private static String type;
    public static String nextType;
    public static DBObject nextPost , nextPicture ;

    public static void setParser(Parser parser) {
        MainManager.parser = parser;
    }

    public static JsonParser getJsonParser() {
        return jsonParser;
    }

    public static void setRequest(Request request) {
        MainManager.request = request;
        Process.request = request;
        MainManager.type = request.getRequestType();
    }

    public static void setType(String newType) {
        MainManager.type = newType;
    }

    public MainManager() throws UnknownHostException {

    }


    public static void main(byte[] bytes , NetworkManager networkManager) {

        parser.handle(bytes);

        if (parser.equals(jsonParser))
        {
            System.out.println("request : " + new String(bytes));
        }

        switch (type) {
            case "signUp": {
                Process.signUp(networkManager);
                break;
            }
            case "logIn": {
                Process.logIn(networkManager);
                break;
            }
            case "logOut" :
            {
                Process.logOut(networkManager) ;
                break;
            }
            case "setting": {
                Process.setting(networkManager);
                break;
            }
            case "profilePage": {

                Process.profilePage(networkManager);
                break;
            }
            case "heartBeat": {
                Process.heartBeat(networkManager);
                break;
            }
            case "saveSetting": {
                Process.saveSetting(networkManager);
                break;
            }
            case "changePassword": {
                Process.changePassword(networkManager);
                break;
            }
            case "search":
            case "directSearch" : {
                Process.search(networkManager);
                break;
            }
            case "othersPage": {
                Process.othersPage(networkManager);
                break;
            }
            case "follow": {
                Process.follow(networkManager);
                break;
            }
            case "unfollow": {
                Process.unfollow(networkManager);
                break;
            }
            case "block": {
                Process.block(networkManager);
                break;
            }
            case "unblock": {
                Process.unblock(networkManager);
                break;
            }
            case "showFollowers": {
                Process.showFollowers(networkManager);
                break;
            }
            case "showFollowing": {
                Process.showFollowing(networkManager);
                break;
            }
            case "showBlockeds": {
                Process.showBlockeds(networkManager);
                break;
            }
            case "newPost": {

                MainManager.nextType = "newPostBytes" ;
                setParser(byteParser);
                MainManager.nextPost = Process.newPost(networkManager);

                break;
            }
            case "newPostBytes" :
            {

                Process.newPostBytes(nextPost , bytes , networkManager) ;
                setParser(jsonParser);
                break;
            }
            case "showPost" :
            {
                Process.showPost(networkManager);
                break;
            }
            case "homePage" :
            {
                Process.homePage(networkManager);
                break;
            }
            case "like" :
            {
                Process.like(networkManager) ;
                break;
            }
            case "unlike" :
            {
                Process.unlike(networkManager) ;
                break;
            }
            case "showNotifications" :
            {
                Process.showNotifications(networkManager);
                break;
            }
            case "message" :
            {
                Process.message(networkManager);
                break;
            }
            case "newMessage" :
            {
                Process.newMessage(networkManager);
                break;
            }
            case "showDirect" :
            {
                Process.showDirect(networkManager);
                break;
            }
            case "accept" :
            {
                Process.accept(networkManager);
                break;
            }
            case "reject" :
            {
                Process.reject(networkManager);
                break;
            }
            case "showComments" :
            {
                Process.showComments(networkManager);
                break;
            }
            case "newComment" :
            {
                Process.newComment(networkManager);
                break;
            }
            case "showLikes" :
            {
                Process.showLikes(networkManager) ;
                break;
            }
            case "forgotPassword" :
            {
                Process.forgotPassword(networkManager) ;
                break;
            }
            case "newProfilePicture" :
            {
                MainManager.nextType = "newProfilePictureBytes" ;
                setParser(byteParser);
                MainManager.nextPicture = Process.newProfilePicture(networkManager);

                break;
            }
            case "newProfilePictureBytes" :
            {
                Process.newProfilePictureBytes(nextPicture , bytes , networkManager) ;
                setParser(jsonParser);
                break;
            }
            default: {
                Process.heartBeat(networkManager);
            }
        }
    }






}
