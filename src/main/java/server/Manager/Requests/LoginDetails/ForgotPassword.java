package server.Manager.Requests.LoginDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class ForgotPassword extends Requests
{

    public ForgotPassword(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }

    public String response(String response)
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
            "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
            "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
            "\",\"" + RequestDetails.response.name() + "\":\"" + response + "\"}";
    }

    public String  check(boolean exist)
    {
        String response = "Your Password was sent to your email !";
        setValid(true);

        if (!exist)
        {
            setValid(false);
            response = "This username does not exist !";
        }

        return response;
    }
}
