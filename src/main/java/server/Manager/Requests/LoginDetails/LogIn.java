package server.Manager.Requests.LoginDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class LogIn extends Requests {
    private String password ;

    public LogIn(long requestID, String requestType, String ownerUsername, String password) {
        super(requestID, requestType, ownerUsername);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String response ( String response)
    {
            return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                    "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                    "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                    "\",\"" + RequestDetails.response.name() + "\":\"" + response +
                    "\",\"" + RequestDetails.successful.name() + "\":" + isValid() + "}";
    }
    public String check(boolean exist, boolean passwordMatch)
    {
        setValid(true);
        String responseString = "";
        if (!exist)
        {
            setValid(false);
            responseString = "This username does not seem to exist!";
        }
        else if (!passwordMatch)
        {
            setValid(false);
            responseString = "Username and password don't match!";
        }
        return  responseString;
    }
}
