package server.Manager.Requests.LoginDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

import java.util.regex.Pattern;

public class SignUp extends Requests {

    private String email ;
    private String password ;
    private String confirmPassword ;

    public SignUp(long requestID, String requestType, String ownerUsername, String email, String password, String confirmPassword)  {
        super(requestID, requestType, ownerUsername);
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public boolean confirm ()
    {
        return (password.equals(confirmPassword) ) ;
    }
    public boolean checkEmail()
    {
        return Pattern.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+.com", getEmail());
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String response (String response )
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.response.name() + "\":\"" + response +
                "\",\"" + RequestDetails.successful.name() + "\":" + isValid() + "}";
    }

    public String check (boolean taken)
    {
        setValid(true);
        String responseString = "";
        password = password.replaceAll(" ", "");

        if (taken)
        {
            responseString = "This username is already taken!";
            setValid(false);
        }
        else if (!confirm())
        {
            responseString = "Entered passwords doesn't match!";
            setValid(false);
        }
        else if (password.length() < 8 || getOwnerUsername().length() < 5)
        {
            responseString = "Username or password is too short!";
            setValid(false);
        }
        else if (!checkEmail()) {
            responseString = "Email is invalid!";
            setValid(false);
        }
        return responseString;
    }

    }
