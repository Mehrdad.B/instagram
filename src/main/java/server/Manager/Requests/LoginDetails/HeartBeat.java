package server.Manager.Requests.LoginDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class HeartBeat extends Requests {


    public HeartBeat(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }

    public String response ( boolean valid )
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.successful.name() + "\":" + valid + "}";

    }

}
