package server.Manager.Requests.OonstagramDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class OthersPage extends Requests
{
    String otherUsername;
    boolean visible ;
    String blockLabel  ;
    String followLabel ;

    public OthersPage(long requestID, String requestType, String ownerUsername, String otherUsername) {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
        blockLabel = "Block" ;
        followLabel = "Follow" ;
        visible = false ;
    }


    public void check (boolean privacy , boolean follow , boolean blockOwner , boolean blockOther , boolean requested  )
    {

        if ( !privacy )
        {
            visible = true ;
        }
        if ( follow )
        {
            visible = true ;
            followLabel = "Unfollow" ;
        }
        if ( blockOwner )
        {
            visible = false;
            blockLabel = "Unblock" ;
            followLabel = "Follow";
        }
        if ( blockOther )
        {
            visible = false;
        }
        if ( requested )
        {
            followLabel = "Requested" ;
        }


    }

    public String response(String bio, int followersNumber, int followingNumber, int postsNumber )
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.bio.name() + "\":\"" + bio +
                "\",\"" + RequestDetails.followersNumber.name() + "\":" + followersNumber +
                ",\"" + RequestDetails.followingNumber.name() + "\":" + followingNumber +
                ",\"" + RequestDetails.postsNumber.name() + "\":" + postsNumber +
                ",\"" + RequestDetails.visible.name() + "\":" + visible  +
                ",\"" + RequestDetails.successful.name() + "\":" + isValid() +
                ",\"" + RequestDetails.blockLabel.name() + "\":\"" +  blockLabel +
                "\",\"" + RequestDetails.followLabel.name() + "\":\"" + followLabel + "\"}";

    }
}
