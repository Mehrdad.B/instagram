package server.Manager.Requests.OonstagramDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

import java.util.ArrayList;
import java.util.List;

public class ShowNotifications extends Requests {
    public ShowNotifications(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }


    public String response ( String jsonList )
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.list.name() + "\":" + jsonList + "}" ;
    }
}
