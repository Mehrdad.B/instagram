package server.Manager.Requests.OonstagramDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

import java.util.List;

public class Search extends Requests
{
    String search;

    public Search(long requestID, String requestType, String ownerUsername, String search) {
        super(requestID, requestType, ownerUsername);
        this.search = search;
    }

    public String response(String response, String jsonArray)
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
            "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
            "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
            "\",\"" + RequestDetails.search.name() + "\":\"" + search +
            "\",\"" + RequestDetails.response.name() + "\":\"" + response +
            "\",\"" + RequestDetails.list.name() + "\":" + jsonArray +
            ",\"" + RequestDetails.successful.name() +"\":" + isValid() + "}";
    }


    public String check(boolean exist, boolean empty)
    {
        String response = "";
        setValid(true);

        if (!exist)
        {
            setValid(false);
            response = "This username does not exist !";
        }
        if (empty)
        {
            setValid(false);
            response = "Try again !";
        }

        return response;
    }
}
