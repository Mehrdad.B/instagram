package server.Manager.Requests.OonstagramDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class HomePage extends Requests
{
    private String color;

    public HomePage(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }

    public String response(String caption, long likeNumber, String otherUsername , long postID)
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.caption.name() + "\":\"" + caption +
                "\",\"" + RequestDetails.color.name() + "\":\"" + color +
                "\",\"" + RequestDetails.postID.name() + "\":" + postID +
                ",\"" + RequestDetails.likeNumber.name() + "\":" + likeNumber + "}";
    }

    public String endResponse()
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"homePageEnded\"}";
    }

    public void check(boolean exist)
    {
        if (exist)
        {
            color = "#fa1304";
        }
        else
        {
            color = "#030303";
        }
    }
}
