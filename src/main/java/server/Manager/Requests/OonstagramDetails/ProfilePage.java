package server.Manager.Requests.OonstagramDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class ProfilePage extends Requests
{

    public ProfilePage(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }

    public String response(String bio, int followersNumber, int followingNumber, int postsNumber)
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.bio.name() + "\":\"" + bio +
                "\",\"" + RequestDetails.followersNumber.name() + "\":" + followersNumber +
                ",\"" + RequestDetails.followingNumber.name() + "\":" + followingNumber +
                ",\"" + RequestDetails.postsNumber.name() + "\":" + postsNumber +
                ",\"" + RequestDetails.successful.name() + "\":" + isValid() +  "}";
    }
}
