package server.Manager.Requests.PostDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class NewPost extends Requests
{
    String caption;

    public NewPost(long requestID, String requestType, String ownerUsername, String caption) {
        super(requestID, requestType, ownerUsername);
        this.caption = caption;
    }

    public String response()
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.response.name() + "\":\"" + "Ready !" + "\"}";
    }
}
