package server.Manager.Requests.PostDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class ShowLikes extends Requests {

    private long postID ;
    private String otherUsername;

    public ShowLikes(long requestID, String requestType, String ownerUsername,String otherUsername, long postID) {
        super(requestID, requestType, ownerUsername);
        this.postID = postID;
        this.otherUsername = otherUsername;
    }

    public String response ( String jsonArray )
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.postID.name() + "\":" + postID +
                ",\"" + RequestDetails.list.name() + "\":" + jsonArray + "}" ;
    }
}
