package server.Manager.Requests.PostDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class ShowPost extends Requests
{
    private String color;

    public ShowPost(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }

    public String response(String caption, long likeNumber, long postID)
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.caption.name() + "\":\"" + caption +
                "\",\"" + RequestDetails.postID.name() + "\":" + postID +
                ",\"" + RequestDetails.color.name() + "\":\"" + color +
                "\",\"" + RequestDetails.likeNumber.name() + "\":" + likeNumber + "}";
    }

    public String endResponse()
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"postsEnded\"}";
    }

    public void check(boolean exist)
    {
        if (exist)
        {
            color = "#fa1304";
        }
        else
        {
            color = "#030303";
        }
    }
}
