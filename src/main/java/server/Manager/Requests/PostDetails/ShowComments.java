package server.Manager.Requests.PostDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class ShowComments extends Requests
{
    long postID;
    String otherUsername;

    public ShowComments(long requestID, String requestType, String ownerUsername, String otherUsername , long postID) {
        super(requestID, requestType, ownerUsername);
        this.postID = postID;
        this.otherUsername = otherUsername;
    }

    public String response( String jsonList )
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.list.name() + "\":" + jsonList +
                ",\"" + RequestDetails.postID.name() + "\":" + postID + "}";
    }
}
