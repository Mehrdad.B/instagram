package server.Manager.Requests.PostDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class NewPostBytes extends Requests {
    private byte[] imageBytes ;


    public NewPostBytes(long requestID, String requestType, String ownerUsername, byte[] imageBytes) {
        super(requestID, requestType, ownerUsername);
        this.imageBytes = imageBytes;
    }

    public String response() {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.response.name() + "\":\"" + "Post saved !" + "\"}";
    }


}
