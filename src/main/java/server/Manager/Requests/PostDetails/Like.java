package server.Manager.Requests.PostDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class Like extends Requests {
    private String otherUsername ;
    private long postID ;

    public Like(long requestID, String requestType, String ownerUsername, String otherUsername, long postID) {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
        this.postID = postID;
    }

    public String response (long likeNumber)
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.likeNumber.name() + "\":" + likeNumber + "}";
    }
}
