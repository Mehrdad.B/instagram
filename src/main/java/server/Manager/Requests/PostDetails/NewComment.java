package server.Manager.Requests.PostDetails;

import server.Manager.Requests.Requests;

public class NewComment extends Requests
{
    long postID;
    String otherUsername;

    public NewComment(long requestID, String requestType, String ownerUsername, String otherUsername , long postID) {
        super(requestID, requestType, ownerUsername);
        this.postID = postID;
        this.otherUsername = otherUsername;
    }
}
