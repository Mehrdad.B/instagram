package server.Manager.Requests.DirectDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class Message extends Requests
{
    String otherUsername;

    public Message(long requestID, String requestType, String ownerUsername, String otherUsername) {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
    }

    public String response (String jsonList , String response)
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.response.name() + "\":\"" + response +
                "\",\"" + RequestDetails.successful.name() + "\":" + isValid() +
                ",\"" + RequestDetails.list.name() + "\":" + jsonList + "}" ;
    }

    public String check(boolean blocked , boolean selfChat)
    {
        setValid(true);

        String response = "";

        if (selfChat)
        {
            setValid(false);
            response = "You cant message yourself";
        }
        if (blocked)
        {
            setValid(false);
            response = "Unblock this user to message them";
        }

        return response;
    }
}
