package server.Manager.Requests.DirectDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class ShowDirect extends Requests
{
    public ShowDirect(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }

    public String response(String jsonArray)
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.list.name() + "\":" + jsonArray + "}";
    }
}
