package server.Manager.Requests.DirectDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class NewMessage extends Requests
{
    private String otherUsername;
    private String text;

    public NewMessage(long requestID, String requestType, String ownerUsername, String otherUsername, String text) {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
        this.text = text;
    }

    public String response(String jsonList)
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.list.name() + "\":" + jsonList + "}";
    }
}
