package server.Manager.Requests.SettingDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class NewProfilePictureBytes extends Requests {
   private byte[] picture ;

    public NewProfilePictureBytes(long requestID, String requestType, String ownerUsername, byte[] picture) {
        super(requestID, requestType, ownerUsername);
        this.picture = picture;
    }

    public String response() {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.response.name() + "\":\"" + "Picture saved !" + "\"}";
    }
}
