package server.Manager.Requests.SettingDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class Setting extends Requests
{
    public Setting(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }

    public String response(String bio, boolean privacy)
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.bio.name() + "\":\"" + bio +
                "\",\"" + RequestDetails.privacy.name() + "\":" + privacy + "}";
    }
}
