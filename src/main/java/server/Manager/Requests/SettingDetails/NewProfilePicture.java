package server.Manager.Requests.SettingDetails;


import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class NewProfilePicture extends Requests {

    public NewProfilePicture(long requestID, String requestType, String ownerUsername) {
        super(requestID, requestType, ownerUsername);
    }
    public String response()
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.response.name() + "\":\"" + "Ready !" + "\"}";
    }
}
