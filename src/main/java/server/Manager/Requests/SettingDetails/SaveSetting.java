package server.Manager.Requests.SettingDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class SaveSetting extends Requests
{
    private String bio;
    private String newUsername;
    private boolean privacy;
    public SaveSetting(long requestID, String requestType, String ownerUsername, String newUsername, String bio, boolean privacy) {
        super(requestID, requestType, ownerUsername);
        this.bio = bio;
        this.newUsername = newUsername;
        this.privacy = privacy;
    }

    public String getBio() {
        return bio;
    }
    public String getNewUsername() {
        return newUsername;
    }
    public boolean getPrivacy() {
        return privacy;
    }

    public String response(String response, String username)
    {
            return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                    "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                    "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + username +
                    "\",\"" + RequestDetails.response.name() + "\":\"" + response +
                    "\",\"" + RequestDetails.bio.name() + "\":\"" + bio +
                    "\",\"" + RequestDetails.privacy.name() + "\":" + privacy + "}";
    }

    public String check(boolean exist)
    {
        setValid(true);
        String responseString = "Information successfully changed!";
        if (!getOwnerUsername().equals(newUsername))
        {
           if (exist)
           {
               setValid(false);
               responseString = "This username is already taken!";
           }
           else if (newUsername.length() < 5)
           {
               setValid(false);
               responseString = "This username is too short!";
           }
        }
        return responseString;
    }
}
