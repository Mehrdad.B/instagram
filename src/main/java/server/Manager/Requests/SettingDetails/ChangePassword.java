package server.Manager.Requests.SettingDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class ChangePassword extends Requests
{
    private String password;
    private String newPassword1;
    private String newPassword2;

    public ChangePassword(long requestID, String requestType, String ownerUsername, String password, String newPassword1, String newPassword2) {
        super(requestID, requestType, ownerUsername);
        this.password = password;
        this.newPassword1 = newPassword1;
        this.newPassword2 = newPassword2;
    }

    public String response(String response)
    {
        return "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.response.name() + "\":\"" + response +
                "\",\"" + RequestDetails.successful.name() + "\":" + isValid() + "}";

    }


    public String check(boolean match)
    {
        setValid(true);
        String response = "Password successfully changed !";
        if (!match)
        {
            setValid(false);
            response = "Wrong password !";
        }
        else if (newPassword1.equals(password) || newPassword2.equals(password))
        {
            setValid(false);
            response = "You entered your old password DADASH !";
        }
        else if (!newPassword1.equals(newPassword2))
        {
            setValid(false);
            response = "Entered new passwords don't match !";
        }
        else if (newPassword1.length() < 8)
        {
            setValid(false);
            response = "New password too short !";
        }
        return response;
    }
}
