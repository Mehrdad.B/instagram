package server.Manager.Requests;

public class Requests  {
    private long requestID ;
    private String requestType ;
    private String ownerUsername ;
    private boolean valid;


    public Requests(long requestID, String requestType, String ownerUsername)  {
        super();

            this.requestID = requestID;
            this.requestType = requestType;
            this.ownerUsername = ownerUsername;
    }

    public long getRequestID() {
        return requestID;
    }

    public String getRequestType() {
        return requestType;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public boolean isValid() {
        return valid;
    }
    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
