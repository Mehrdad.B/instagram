package server.Manager.Requests.ProfileDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class Block extends Requests {
    private String otherUsername ;

    public Block (long requestID, String requestType, String ownerUsername, String otherUsername) {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
    }
    public String response ()
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername + "\"}" ;
    }

}
