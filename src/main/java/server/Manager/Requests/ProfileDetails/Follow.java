package server.Manager.Requests.ProfileDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class Follow extends Requests
{
    private String otherUsername ;

    public Follow(long requestID, String requestType, String ownerUsername, String otherUsername) {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
    }

    public String check ( boolean block  , boolean Privacy )
    {
        String response = "Unfollow" ;
        setValid(true);
        if ( block || Privacy )
        {
            response = "Requested" ;
            setValid(false);
        }
        return response ;

    }
    public String response ( String response )
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.followLabel.name() + "\":\"" + response + "\"}" ;
    }


}
