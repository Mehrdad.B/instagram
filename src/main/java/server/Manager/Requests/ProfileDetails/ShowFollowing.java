package server.Manager.Requests.ProfileDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class ShowFollowing extends Requests {

    private String otherUsername ;

    public ShowFollowing(long requestID, String requestType, String ownerUsername, String otherUsername) {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
    }
    public String response ( String jsonArray )
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.ownerUsername.name() + "\":\"" + getOwnerUsername() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.list.name() + "\":" + jsonArray + "}" ;
    }


}
