package server.Manager.Requests.ProfileDetails;

import server.JsonHandler.RequestDetails;
import server.Manager.Requests.Requests;

public class Unfollow extends Requests
{
    private String otherUsername ;

    public Unfollow(long requestID, String requestType, String ownerUsername, String otherUsername) {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
    }

    public String response ( boolean privacy )
    {
        return  "{\"" + RequestDetails.requestID.name() + "\":\"" + getRequestID() +
                "\",\"" + RequestDetails.requestType.name() + "\":\"" + getRequestType() +
                "\",\"" + RequestDetails.otherUsername.name() + "\":\"" + otherUsername +
                "\",\"" + RequestDetails.response.name() + "\":\"" +  "Follow"  +
                "\",\"" + RequestDetails.privacy.name() + "\":\"" + privacy + "\"}" ;
    }
}
