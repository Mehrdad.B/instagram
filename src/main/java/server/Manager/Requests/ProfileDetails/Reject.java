package server.Manager.Requests.ProfileDetails;

import server.Manager.Requests.Requests;

public class Reject extends Requests
{
    private String otherUsername;

    public Reject(long requestID, String requestType, String ownerUsername, String otherUsername)
    {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
    }
}
