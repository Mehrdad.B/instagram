package server.Manager.Requests.ProfileDetails;

import server.Manager.Requests.Requests;

public class Accept extends Requests
{
    private String otherUsername;

    public Accept(long requestID, String requestType, String ownerUsername, String otherUsername)
    {
        super(requestID, requestType, ownerUsername);
        this.otherUsername = otherUsername;
    }
}
