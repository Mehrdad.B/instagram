package server.Manager.Notifications;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.JsonHandler.RequestDetails;
import server.Manager.DataBase.FieldDetails;

public class FollowRequestNotif extends Notifications{

    public FollowRequestNotif(String senderUsername, String receiverUsername) {
        super(senderUsername, receiverUsername);
    }

    String text = getSenderUsername() +  " requested to follow you . "  ;

    public String notification (String jsonArray)
    {
        return "{\"" + RequestDetails.requestType.name() + "\":\"" + RequestDetails.notification.name() +
                "\",\"" + NotifDetails.notificationType.name() + "\":\"" + NotifDetails.followRequest.name()+
                "\",\"" + NotifDetails.otherUsername.name() + "\":\"" + getSenderUsername() +
                "\",\"" + NotifDetails.ownerUsername.name()  + "\":\"" + getReceiverUsername() +
                "\",\"" + NotifDetails.list.name() +"\":" + jsonArray +  "}" ;
    }
    public DBObject getDBObject ()
    {
        return new BasicDBObject()
                .append(FieldDetails.notificationType.name(), FieldDetails.followRequestNotif.name())
                .append(FieldDetails.senderUsername.name(), getSenderUsername())
                .append(FieldDetails.receiverUsername.name() , getReceiverUsername())
                .append(FieldDetails.text.name(), text) ;
    }

    public void OnlineNotif ()
    {

    }
}
