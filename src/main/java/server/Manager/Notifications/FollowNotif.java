package server.Manager.Notifications;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.JsonHandler.RequestDetails;
import server.Manager.DataBase.FieldDetails;

public class FollowNotif extends Notifications {

    public FollowNotif(String senderUsername, String receiverUsername) {
        super(senderUsername, receiverUsername);
    }
    String text = getSenderUsername() + " started following you ." ;
    public String notification (String jsonArray)
    {
        return "{\"" + RequestDetails.requestType.name() + "\":\"" + RequestDetails.notification.name() +
                "\",\"" + NotifDetails.notificationType.name() + "\":\"" + NotifDetails.follow.name()+
                "\",\"" + NotifDetails.otherUsername.name() + "\":\"" + getSenderUsername() +
                "\",\"" + NotifDetails.ownerUsername.name()  + "\":\"" + getReceiverUsername() +
                "\",\"" + NotifDetails.list.name() +"\":" + jsonArray +  "}" ;
    }
    public DBObject getDBObject ()
    {
        return new BasicDBObject()
                .append(FieldDetails.notificationType.name(), FieldDetails.followNotif.name())
                .append(FieldDetails.senderUsername.name(), getSenderUsername())
                .append(FieldDetails.receiverUsername.name() , getReceiverUsername())
                .append(FieldDetails.text.name(), text) ;
    }

    public void OnlineNotif ()
    {

    }
}
