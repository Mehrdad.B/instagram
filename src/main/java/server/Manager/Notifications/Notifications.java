package server.Manager.Notifications;

public class Notifications {


    private String senderUsername ;
    private String receiverUsername ;


    public Notifications( String senderUsername, String receiverUsername) {

        this.senderUsername = senderUsername;
        this.receiverUsername = receiverUsername;
    }



    public String getReceiverUsername() {
        return receiverUsername;
    }

    public String getSenderUsername() {
        return senderUsername;
    }

}
