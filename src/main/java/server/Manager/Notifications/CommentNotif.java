package server.Manager.Notifications;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.JsonHandler.RequestDetails;
import server.Manager.DataBase.FieldDetails;

public class CommentNotif extends Notifications {

    public CommentNotif(String senderUsername, String receiverUsername) {
        super(senderUsername, receiverUsername);
    }

    String text = getSenderUsername() + " commented on your post . " ;

    public String notification (String jsonArray)
    {
        return "{\"" + RequestDetails.requestType.name() + "\":\"" + RequestDetails.notification.name() +
                "\",\"" + NotifDetails.notificationType.name() + "\":\"" + NotifDetails.comment.name()+
                "\",\"" + NotifDetails.otherUsername.name() + "\":\"" + getSenderUsername() +
                "\",\"" + NotifDetails.ownerUsername.name()  + "\":\"" + getReceiverUsername() +
                "\",\"" + NotifDetails.list.name() +"\":" + jsonArray +  "}" ;
    }

    public DBObject getDBObject ()
    {
        return new BasicDBObject()
                .append(FieldDetails.notificationType.name(), FieldDetails.commentNotif.name())
                .append(FieldDetails.senderUsername.name(), getSenderUsername())
                .append(FieldDetails.receiverUsername.name() , getReceiverUsername())
                .append(FieldDetails.text.name(), text) ;
    }

    public void OnlineNotif ()
    {

    }
}
