package server.Manager.Notifications;

import javafx.beans.property.adapter.JavaBeanDoublePropertyBuilder;

public enum NotifDetails {

    like , follow , followRequest , comment ,

    notificationType , senderUsername , receiverUsername , text , direct , list ,

    otherUsername , ownerUsername ;
}
