package server.Manager.Notifications;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.JsonHandler.RequestDetails;
import server.Manager.DataBase.FieldDetails;

public class LikeNotif extends Notifications{

    public LikeNotif(String senderUsername, String receiverUsername) {
        super(senderUsername, receiverUsername);
    }

    private  String text = getSenderUsername() + " liked your post. " ;
    public String notification (String jsonArray)
    {
        return "{\"" + RequestDetails.requestType.name() + "\":\"" + RequestDetails.notification.name() +
                "\",\"" + NotifDetails.notificationType.name() + "\":\"" + NotifDetails.like.name()+
                "\",\"" + NotifDetails.otherUsername.name() + "\":\"" + getSenderUsername() +
                "\",\"" + NotifDetails.ownerUsername.name()  + "\":\"" + getReceiverUsername() +
                "\",\"" + NotifDetails.list.name() +"\":" + jsonArray +  "}" ;
    }
    public DBObject getDBObject ()
    {
        return new BasicDBObject()
                .append(FieldDetails.notificationType.name(), FieldDetails.likeNotif.name())
                .append(FieldDetails.senderUsername.name(), getSenderUsername())
                .append(FieldDetails.receiverUsername.name() , getReceiverUsername())
                .append(FieldDetails.text.name(), text) ;
    }

    public void OnlineNotif ()
    {

    }
}
