package server.Manager.Notifications;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import server.JsonHandler.RequestDetails;
import server.Manager.DataBase.FieldDetails;

public class DirectNotif extends  Notifications{
    private String text ;

    public DirectNotif(String senderUsername, String receiverUsername, String text) {
        super(senderUsername, receiverUsername);
        this.text = text;
    }

    public String notification ( String jsonArray )
    {

        return "{\"" + RequestDetails.requestType.name() + "\":\"" + RequestDetails.notification.name() +
                "\",\"" + NotifDetails.notificationType.name() + "\":\"" + NotifDetails.direct.name()+
                "\",\"" + NotifDetails.otherUsername.name() + "\":\"" + getSenderUsername() +
                "\",\"" + NotifDetails.ownerUsername.name()  + "\":\"" + getReceiverUsername() +
                "\",\"" + NotifDetails.list.name() +"\":" + jsonArray +  "}" ;
    }
    public DBObject getDBObject ()
    {
        return new BasicDBObject()
                .append(FieldDetails.notificationType.name(), FieldDetails.directNotif.name())
                .append(FieldDetails.senderUsername.name(), getSenderUsername())
                .append(FieldDetails.receiverUsername.name() , getReceiverUsername())
                .append(FieldDetails.text.name(), text) ;
    }
}
